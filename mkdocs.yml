site_name: Architecture des SI
site_url: https://domart.frama.io/archisi/ # Permet d'avoir la racine dans l'URL avec "mkdocs serve"
site_dir: public
repo_url: https://domart.frama.io
repo_name: Autres sites

docs_dir: docs

nav:
    - "🏡 Accueil": index.md
    - ... | TD1/*.md
    - ... | TD2/*.md
    - ... | TD3/*.md
    - ... | TD4/*.md
    - ... | TD5/*.md
    - ... | TD6/*.md
    - ... | TD7/*.md
    - ... | TD8/*.md
    #- ... | TD8-2023-2024/*.md
    #- ... | WS/*.md
    - ... | TD9/*.md
    - ... | TD10/*.md
    - ... | TD11/*.md
    - ... | TD12/*.md
    #- ... | TD12-2023-2024/*.md
    #- ... | WS_old/*.md
    - ... | Aides/*.md
    - ... | Corrections/*.md


theme:
    favicon: images/jee.png
    logo: images/jee.png
    icon:
        repo: material/home
    

    name: material
    font: false                     # RGPD ; pas de fonte Google
    language: fr                    # français
    palette:                        # Palettes de couleurs jour/nuit
        # Light mode
        - media: "(prefers-color-scheme: light)"
          scheme: default
          primary: teal
          accent: green
          toggle:
              icon: material/weather-night  #toggle-switch-off-outline
              name: Passer l'affichage en mode sombre

        # Dark mode
        - media: "(prefers-color-scheme: dark)"
          scheme: slate
          primary: grey
          accent: teal
          toggle:
              icon: material/weather-sunny   #toggle-switch
              name: Passer l'affichage en mode clair


    features:
        #- navigation.instant
        - navigation.tabs
        - navigation.expand
        - navigation.top
        - toc.integrate # Permet d'avoir la nav à gauche et la tablea de matières à droite.
        - navigation.indexes # Permet d'associer une page directement à une section.
        #- navigation.sections
        - navigation.tabs.sticky   # Permet de toujours afficher la barre de nav en haut.
        - navigation.footer        # Permet d'ajouter les liens suivants et précédents dans le pied de page.
        #- header.autohide
        - content.code.annotate
        - content.code.copy        # Permet d'avoir le bouton pour copier du code.
        - content.tabs.link


markdown_extensions:
    - meta
    - abbr
    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - md_in_html                    # Pour mettre du markdown dans du HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque" 
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        linenums: None
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Texte"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
            "Java": "☕ Code Java"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox: false      # ...avec cases d'origine
        clickable_checkbox: true    # ...et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
          alternate_style: true
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
          emoji_index:     !!python/name:material.extensions.emoji.twemoji
          emoji_generator: !!python/name:material.extensions.emoji.to_svg
    - pymdownx.superfences:         # Imbrication de blocs.
        custom_fences:              # mermaid permet de faire des diagrammes.
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format
    - pymdownx.betterem:            # Amélioration de la gestion de l'emphase (gras & italique).
          smart_enable: all
    - pymdownx.arithmatex:          # Formules en LaTeX dans le MD, converties en MathJax
          generic: true
    - toc:
          permalink: ⚓︎
          toc_depth: 3


plugins:
    - search
    - awesome-pages:
        collapse_single_pages: true
    - macros



extra_javascript:
    ### Pour latex - Début.
    - xtra/config.js                    # MathJax
    - https://polyfill.io/v3/polyfill.min.js?features=es6
    - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
    ### Pour latex - Fin.

extra_css:
    - css/monStyle.css    # style perso.
    - css/admonition.css    # admonition en plus.