---
author: Benoît Domart
title: Exemple de JSP
tags:
    - 0-aide
---

???+tip "Exemple de JSP"
    Voici un exemple de page JSP, à adapter en fonction de ton besoin :

    ``` jsp linenums="1"
    <%@page import="java.util.List"%><!--(1)!-->
    <html>
    <head>
        <title>Affiche une liste dynamique</title>
        <meta charset="UTF-8"/>
    </head>
    <body>
        <form action="liste" method="post">
            <input type="number" name="taille"
                value="<%=((List<String>)request.getAttribute("LISTE")).size()%>"/><!--(2)!-->
            <input type="submit" name="Valider"/>
        </form>
        <table>
            <%
            List<String> liste = (List<String>)request.getAttribute("LISTE"); //(3)!
            for (String mot : liste) {
            %>
                <tr><td><%=mot %></td></tr>
            <% } %>
        </table>
    </body>
    </html>
    ```

    1. Il s'agit d'une directive JSP.

        Elle permet d'importer un objet Java nécessaire.

    2. Il s'agit d'une expression JSP.

        L'attribut `value` permet de donner une valeur à ce champ à l'affichage de la page. Ici, la valeur sera 0 au premier affichage (la taille de la liste vide), puis celle saisie précédemment aux suivants.
        
    3. Il s'agit d'un scriptlet.
        
        On récupère la liste placée en attribut de la requête, et on affiche autant de ligne dans le tableau qu'il y a d'éléments dans la liste.