---
author: Benoît Domart
title: Choix de l'implémentation
tags:
    - 0-aide
---


???+success "Comment JEE sait-il quelle implémentation choisir s'il y en a plusieurs ?"

    Il suffit de rajouter l'annotation `@Alternative` sur les implémentations qu'on ne souhaite pas utiliser !

    Par exemple, ouvrir le projet suivant :

    <center>
        [Exemple injection - dépôt GIT](https://scm.univ-tours.fr/domart/testinject.git){ .md-button target=_blank }
    </center>

    Ce projet contient :

    - La couche **Contrôleur** : Une *Servlet*, qui appelle la couche métier.
    - La couche **Métier** : Elle contient l'interface et deux classes l'implémentant (`TestBusinessImpl1` et `TestBusinessImpl2`). Il suffit donc de rajouter l'annotation `@Alternative` sur l'implémentation qui ne doit pas être utilisée.

        Cela fonction bien sûr également s'il y a plus de 2 implémentations.