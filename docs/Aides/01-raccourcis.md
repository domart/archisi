---
author: Benoît Domart
title: Quelques raccourcis vscode utiles
tags:
    - 0-aide
---

## 1. Raccourcis par défaut

- ++ctrl+"p"++ : Permet de saisir le nom d'un fichier présent dans le *workspace*.
- ++ctrl+shift+"i"++ : Format le fichier (si un formateur est bien associé à ce type de fichier).
- ++alt+shift+"o"++ : Nettoyage (et ajout) des imports nécessaires.
- ++ctrl+shift+"p"++ : Permet d'exécuter une commande dans VSCode (par exemple un script Maven ...).
- ++ctrl+shift+k++ : Permet de supprimer la ligne courante.
- ++ctrl+shift+;++ : Correction rapide (d'une erreur dans le code, ou d'une faute d'orthographe par exemple).

## 2. Raccourcis personnalisés

- ++ctrl+shift+"a"++ : Permet d'ouvrir le menu **Source Action...**.