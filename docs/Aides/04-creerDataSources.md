---
author: Benoît Domart
title: Créer une DataSource avec WildFly
tags:
    - 0-aide
---


???+tip "Créer une DataSource avec WildFly"

    - Créer une instance sur votre BDD s'appelant `mon_instance`.
    - Depuis la console d'administration WildFly, créer la *datasource* `java:/MaDataSource`. Pour rappel, voici ce qu'il faut faire :

        1. Sélectionner "MySQL"
        2. Indiquer `MaDataSource` comme *Name*, et `java:/MaDataSource` comme *JNDI Name*.
        3. Ne rien changer.
        4. Indiquer `jdbc:mysql://localhost:3306/mon_instance` dans Connection URL (il faut adapter cette URL si le port est différent, et si le schéma créé sur la BDD n'est pas gestion_notes), et saisir le *User Name* et l'éventuel *Password*.
        5. Tester la connexion.
        6. Valider la création de la *datasource*.

        ???+note "Pilote MySql"

            Vous avez déjà créé le pilote MySql, il ne faut donc pas le refaire. La seule opération supplémentaire à faire est ce qui juste ci-dessus.