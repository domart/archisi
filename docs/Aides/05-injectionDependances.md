---
author: Benoît Domart
title: Rappel succinct des injections de dépendances
tags:
    - 0-aide
---


???+tip "Rappel succinct des injections de dépendances"

    Nous avons deux "dépendances" à gérer :

    1. De chaque ***servlet*** vers les **services métiers** qu'elle souhaite appeler.
    2. De chaque **service métier** vers les différents **services DAO** qu'elle souhaite appeler.

    Dans tous les cas, on doit avoir une dépendance faible. C'est-à-dire que dans la classe cliente, on doit indiquer uniquement l'**interface** correspondant au service appelée.

    Il ne doit donc pas y avoir d'instanciation explicite dans les classe appelante, pas de `new ServiceMachinChoseImpl()`. L'instanciation de ces services se fait par **injection de dépendance**, via l'utilisation de l'annotation `@Inject`.

    Pour que l'injection soit opérationnelle, il faut (ou plutôt il suffit) qu'il y ait l'annotation `@Stateless` sur les implémentations concernées.