---
author: Benoît Domart
title: Les annotations utiles
tags:
    - 0-aide
---


???+tip "Les annotations utiles"

    Voici quelques annotations que vous devez savoir utiliser :

    - `@Stateless` : À mettre sur la classe en question.
    
        Signifie que cette classe pourra être injectée en tant que dépendances.
    
    - `@Inject` : À mettre sur un attribut de classe.

        Cet attribut doit être une interface.

        Permet d'effectuer l'injection de dépendances, c'est-à-dire que WildFly va sélectionner la bonne classe pour implémenter cette interface (celle qui a le `@Stateless`).
    
    - `@WebServlet` : À mettre sur la classe en question.

        Signifie que cette classe est une Servlet. Il **faut** également que cette classe étende `HttpServlet`.

    - `@PersistenceContext` : À mettre sur l'attribut de classe `EntityManager`, sur chaque classe implémentant un DAO.

        ```java title=""
        @PersistenceContext(unitName = "GestionNotes")
        private EntityManager em;
        ```

        Permet d'indiquer quelle unité de persistence utiliser (définie dans le fichier `persistence.xml`).
    
    - `@Entity` : À mettre sur la classe en question.

        Signifie que la classe, qui doit être un bean, correspond à une table en base de données.

        Par défaut, la table aura le même nom que la classe. Si on souhaite que la table ait un autre nom, on peut utiliser l'annotation `@Table(name = "NOTE_JPA")`.
    
    - `@Id` : À mettre sur l'attribut de classe d'un bean.

        ```java title=""
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        ```

        Signifie que l'attribut en question est une clef primaire. On spécifie de plus ici qu'il sera auto-incrémenté.