---
author: Benoît Domart
title: Créer un raccourci personnalisé
tags:
    - 0-aide
---


???+tip "Ajouter des raccourcis à vscode"

    Par défaut, il n'y a aucun raccourci affecté à l'affichage de `Source Action...`. Mais il est possible d'en rajouter un !

    Pour cela, il faut ouvrir le fichier `keyBindings.json`, qui recense tout vos raccourcis personnalisés.

    - Si vous ne l'avez jamais ouvert, il faut faire ++ctrl+"k"++ ; ++ctrl+"s"++, puis cliquer sur l'icône tout en haut à droite pour afficher le fichier JSON.
    - La prochaine fois, il suffira de faire ++ctrl+"p"++, puis saisir `keybindings.json`.

    Dans ce fichier, ajouter le code suivant pour que le raccourci ++ctrl+shit+"a" permette cette action :

    ```json
    // Placer vos combinaisons de touches dans ce fichier pour remplacer les valeurs par défaut
    [
        // Raccourcis précédents ...
        {
            "key": "ctrl+shift+a",
            "command": "editor.action.sourceAction"
        },
        // Raccourcis suivants ...
    ]
    ```