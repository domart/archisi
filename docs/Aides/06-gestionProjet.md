---
author: Benoît Domart
title: Rappel gestion projet maven
tags:
    - 0-aide
---


???+tip "Rappel gestion projet maven"

    1. On commence donc par créer un projet Web :

        Pour cela, il faut aller fans la vue **MAVEN** et cliquer sur le bouton "**+**". Dans la zone de saisie, après avoir cliqué sur `more` (attention ! il faut cliquer sur `more`, sinon vous ne pourrez pas sélectionner toutes les options nécessaires !), sélectionner `webapp-jakartaee10`.

        C'est toujours cet *archetype* que nous utiliserons dans ce cours.

        Si la vue **MAVEN** n'apparaît, pas, lance l'exécuteur de commande, via ++ctrl+shift+"P"++, puis saisir `Maven: New Project...`,

    1. Après avoir validé, sélectionner la dernière version (1.1.0 au moment où ceci est rédigé), puis indiquer le **group id** de notre projet, c'est-à-dire l'identifiant de la structure à laquelle le projet appartient. Ici, nous allons indiquer `fr.univtours.polytech`. Il s'agit du domaine internet de la structure (sans tiret), écrit à l'envers.

    1. Il faut ensuite indiquer le nom du projet. Par exemple `monprojet`.

        Tous les packages de notre projet commencerons donc par `fr.univtours.polytech.monprojet`.

    1. Il faut maintenant indiquer où le projet, c'est-à-dire ici le dossier 📂`monprojet`, sera stocké sur le disque dur.

        Il faut donc indiquer 📂`JEE/ws_archisi`.

    1. Enfin, pour terminer, dans le terminal de **vscode**, il faut valider certains choix : le numéro de version de notre projet et le début des packages (il suffit de ne rien saisir et de valider à chaque fois pour conserver les valeurs par défaut, qui nous conviennent).

    1. Pour que le nom du livrable (le WAR) ne soit pas trop compliqué, il faut aller dans le 📄`pom.xml` et ajouter, à l'intérieur de la balise `<build>`, la balise suivante :

        ```xml title="Dans le 📄pom.xml"
        <finalName>monprojet</finalName>
        ```

        C'est le nom qui sera donné au WAR : 📄`monprojet.war`, et la racine de contexte qui sera utilisée.
    
    1. Dans le dossier 📂`target` créé par Maven, cliquer droit sur 📄`monprojet.war` puis sélectionne `Run on server`. Le projet est alors déployé sur le serveur.

    2. Le nom du WAR est la racine de contexte, c'est-à-dire que toutes les URL pour accéder aux pages de notre application commencerons par `http://localhost:8080/monprojet/`.

    1. Pour rappel, vous pouvez choisir d'exécuter une ressource (c'est-à-dire pour nous une Servlet) par défaut avec cette URL.

        Il faut pour cela aller dans le 📄`web.xml` et ajouter les balises suivantes :

        ```xml title="Dans le 📄web.xml"
        <welcome-file-list>
            <welcome-file>urlDeMaServletParDefaut</welcome-file>
        </welcome-file-list>
        ```


    
    Pour rappel, **à chaque modification de votre code**, il faut réexécuter la commande

    ``` title=""
    mvn clean install
    ```

    à partir du dossier contenant le fichier 📄`pom.xml`.

    Une fois que le WAR est généré, il est supprimé du serveur WildFly, puis déployé à nouveau. C'est pour cela que si vous rafraîchissez la page dans votre navigateur, vous pouvez à un moment obtenir une erreur 404.