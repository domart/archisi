---
author: Benoît Domart
title: Les images
---

# Gérer les images

Les images peuvent être stockées dans une base de données. On utilise pour cela le type `BLOB`, pour *Binary Large OBject*. Pour utiliser cela, une façon de faire (parmi d'autres) est :

1. Dans le modèle :

    - ajouter un attribut `byte[]` avec l'annotation `@LOB`, ainsi que les getter/setter associés,
    - ajouter une méthode permettant de transformer le `byte[]` en `String`, pour l'affichage dans la vue.

    Cela donne, par exemple :

    ```java
    @Lob
    private byte[] picture;

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    /**
    * Transforme le byte[] contenant les données de l'image en String.
    * 
    * @return La chaîne de caractère permettant l'affichage d'une image depuis une
    *         JSP.
    */
    public String getBase64Image() {
        return Base64.getEncoder().encodeToString(this.picture);
    }
    ```

1. Dans la vue, pour afficher l'image (avec une EL - *Expression Language*), il faut ajouter :

    ```jsp
    <img src="data:image/jpg;base64,${monObjet.base64Image}" width="200px"/>
    ```

    La méthode `getBase64Image` sera donc appelée sur l'objet `monObjet`.

1. En BDD, pour insérer une image,

    - il faut tout d'abord la positioner dans le dossier `upload`. Pour le trouver, il faut exécuter la commande

        ```sql
        SHOW VARIABLES LIKE "secure_file_priv";
        ```
    
    - Il faut ensuite placer l'image souhaitée dans ce dossier, et utiliser la fonction `LOAD_FILE(path)`, où path est le chemin **absolu** de l'image.
    
        Par exemple, *chez moi*, le dossier `upload` est `C:\ProgramData\MySQL\MySQL Server 8.0\Uploads\`. Cela donne donc quelque chose comme :

        ```sql
        UPDATE table set picture = LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\mon_image.jpg') where id = l_id_souhaité;
        ```
