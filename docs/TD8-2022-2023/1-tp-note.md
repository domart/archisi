---
author: Benoît Domart
title: Bibliothèque
---

# TP noté - Site d'une bibliothèque


???+success "Objectif"
    Modéliser et implémenter une application Web permettant de gérer une bibliothèque en ligne.

???+cdc "Cahier des charges"
    Cette application contient trois parties :
    
    1. Un catalogue public des livres disponibles, avec un formulaire de recherche pour filtrer les livres par auteur, par titre, par genre et/ou par disponibilité (vous pouvez rajouter des critères). Il faut que :

        - La recherche par auteur est un champ de saisie libre, insensible à la casse, et ne peut être qu'une partie du nom de l'auteur.
            Par exemple, la saisie de "ArJa" doit notamment renvoyer tous les livres de "René Barjavel".
        - Idem pour la recherche par titre.
        - La recherche par genre est une liste déroulante, avec tous les genres présents dans la bibliothèque.
        - Pour la disponibilité, une case à cocher doit permettre (si elle est cochée) de ne renvoyer que les livres disponibles. Si elle n'est pas cochée, tous les livres sont renvoyés.

        ***Bonus*** : Vous pouvez afficher l'image du livre dans le résultat de la recherche (Cf. [la page dédiée à cela](../2-image)).

    1. Une partie réservée aux utilisateurs inscrits, nécessitant une connexion.
        
        Ces utilisateurs peuvent alors emprunter un ou plusieurs livres **disponibles**, 5 au maximum. Pour cela, il suffit à l'utilisateur de cliquer sur le livre qu'il souhaite emprunter (dans le résultat de la recherche). Ceci n'est fonctionnel que si l'utilisateur est connecté. L'emprunt est pour 10 jours, à partir du moment où l'utilisateur a cliqué sur le livre.

        ***Bonus*** : Si vous avez le temps, vous pouvez ne pas stocker le mot de passe en base en clair (**ce qu'il ne faut bien sûr jamais faire !**). Pour cela vous pouvez vous inspirer de [cette page](../3-md5).
        
        L'application signale également à l'utilisateur s'il a des documents empruntés, et si oui, depuis combien de temps, et l'alerte s'il doit les rendre bientôt (dans moins d'une semaine) et s'il est retard.

    1. Une partie réservée aux administrateurs de l'application, c'est-à-dire aux bibliothécaires.
        Il y est possible d'indiquer qu'un utilisateur a rendu un livre.

        ***Bonus*** : Si vous avez le temps, vous pouvez implémenter les fonctionnalités de création d'un nouvel utilisateur, et d'ajout d'un livre dans la bibliothèque.
    
???+success "Livrables"

    - Merci de fournir un fichier ZIP ou un dépôt git contenant les sources.
    - Pensez à fournir un script permettant l'insertion des données dans la base une fois celle-ci créée.
    - Vous fournirez également un, ou plusieurs, document(s) décrivant :
        - les spécifications (par exemple toutes les règles de gestion mises en place) du projet,
        - le schéma de la base de données (on pourra utiliser [mocodo](https://www.mocodo.net/){target=_blank}) et
        - l'architecture MVC de l'application.

???+tip "Conseils"

    Avant de vous lancer dans le développement, il faut réfléchir un peu.

    - Commencez par créer le modèle de la BDD,
    - puis listez toutes les règles de gestion à mettre en place,
    - puis listez tous les services que l'application va devoir fournir.
    - puis listez tous les écrans qu'il va falloir créer ...

    Ensuite, essayez de vous répartir le travail de façon à ne pas être trop dépendant de votre binôme ...

    **Tous ces points seront détaillé dans le document à rendre.**

???+tip "Conseils - Suite"

    N'oubliez pas de mettre dans le `persistence.xml` l'option `drop and create`, au moins la première fois, pour que les tables soient créées dans la BDD.

    Une fois que votre modèle est stable, supprimez cette option (mettre `Default (None)` à la place - la valeur par défaut), et créez-vous un script d'insertion de vos données dans les tables correspondantes, afin d'avoir un jeu de test.

    À chaque modification du modèle (c'est-à-dire d'un des *beans*), il faut pensez à remettre l'option `drop and create`, et à l'enlever ensuite.
