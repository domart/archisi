# TP2 - Web Services SOAP - Retour sur les locations

Pour ce TP, nous allons repartir du projet **_Location_** sur lequel nous avions travaillé plus tôt. Nous allons créer un Web Service SOAP permettant d'appeler à distance les services que nous souhaitons exposer.


???+exercice "Exercice 1"

    Modifier le projet EJB de location pour rajouter une méthode permettant de supprimer une location avec comme paramètre l'identifiant de cette location.
    
    
???+exercice "Exercice 2"

    Créer un Web Service SOAP qui expose les méthodes suivantes :
    
    - Récupération des toutes les locations disponibles.
    - Récupération d'une location (à partir de son identifiant).
    - Suppression d'une location (à partir de son identifiant).
    - Création d'une location.
    
    :warning: Attention, il ne faut pas exposer le champ `picture` de `LocationBean`.

    Tester ce Web Service avec SoapUI.