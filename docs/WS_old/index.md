# Mise en place de Web Services

## 1. Installation de Java

???+ note "Installation de la version 1.8 de Java"

	Pour rappel, voici comment installer une version de Java, la 1.8.0_202 ici en l'occurrence :

	1. Télécharger la version [ici](https://www.oracle.com/fr/java/technologies/javase/javase8-archive-downloads.html){:target="_blank"}, en faisant bien attention de prendre celle correspondant à votre système d'exploitation.
	2. Selon le cas, exécuter l'exécutable, ou dezipper l'archive, dans un dossier que nous appelerons 📂`<java_dir>` dans la suite.
	3. Aller dans les variables d'environnements de votre système, et créer (ou modifier) les deux variables d'environnements suivantes :
	
		1. `JAVA_HOME` : lui affecter la valeur 📂`<java_dir>`.
		2. `JRE_HOME` : lui affecter la valeur 📂`<java_dir>`.
	4. Toujours dans les variables d'environnements, modifier la variable d'environnement `PATH`, en rajoutant en premier la valeur `%JAVA_HOME%\bin` (sous Windows), ou `$JRE_HOME/bin` (sous Linux).<br>
		Ainsi, lorsque vous exécutez Java, vous êtes sûr que c'est la version qui pointe vers 📂`<java_dir>`.<br>
		Pour le vérifier, exécuter la commande
		```batch
		java -version
		```
		et vérifier que la réponse est bien `java version "1.8.0_202"`.
	5. Pour eclipse, il faut une version de java supérieure ou égale à 11. Il suffit d'aller là où eclipse est installé, ouvrir le fichier `eclipse.ini`, et rajouter au tout début
		```ini title="📄eclipse.ini"
		-vm
		<java_11_ou_plus_dir>/bin
		```

## 2. Autres outils utiles

???+ note "Les autres outils que nous allons utiliser pour tester les Web Services"

	1. SaopUI open source : [https://www.soapui.org/downloads/soapui/](https://www.soapui.org/downloads/soapui/){:target="_blank"}
	2. Postman (il faut créer un compte) : [https://www.postman.com/downloads/](https://www.postman.com/downloads/){:target="_blank"}
	3. Une version d'eclipse (ou autre IDE) permettant le développement JEE, et contenant les plugins suivants (pour rappel, il faut aller dans `Help > Eclipse Marketplace...` pour les installer) :
		1. JBoss Tools (nous l'avons déjà installé dans la première partie de ce cours).
		2. Spring tools 4