# TP noté

## 1. Prérequis

:warning: Il faut bien faire attention à utiliser la même version de Java partout, pour le runtime WildFly, et pour le projet Java.


## 2. Le contexte

???+success "Le contexte"

    Nous disposons ici d'une application d'e-commerce, qui contient un catalogue de produit, contenants eux-mêmes des commentaires.

    L'objectif est double :

    1. Fournir des Web Services respectant l'architecture Rest permettant d'accéder à ce catalogue (en lecture et en écriture).
    2. Se connecter à un Web Service externe pour convertir les prix dans n'importe quelle devise.
    
    Un produit est constitué des informations suivantes :
    
    - Un identifiant.
    - Une catégorie.
    - Un libellé.
    - Son prix en euro.
    - Le nombre de ce produit en stock.
    
    Un commentaire est constité des informations suivantes :
    
    - Un identifiant.
    - L'identifiant du produit auquel il est rattaché.
    - L'auteur du commentaire.
    - La note que cet auteur a donné au produit.
    - Le commentaire en lui-même.


## 3. Installation

???+success "Installation"

    Si vous utilisez Eclipse, il faut :

    1. Avoir une runtime WildFly qui s'appelle `WildFly 26.0.1 Runtime`.
    2. Vérifier dans le menu `Project` que `Build Automatically` est bien coché.
    
    ___

    Voici ce qu'il faut créer pour faire fonctionner le squelette fourni sur Celene :

    1. Créer une base de données `estore_db` sur votre instance MySql :

        ```sql
        create schema estore_db;
        ```

    2. Sur le serveur WildFly, ajouter une _datasource_, dont le nom JNDI est `java:/EStoreDB`, vers cette base de données :

        1. Soit en la configurant depuis la console d'administration de votre serveur WildFly.
        2. Soit en recopiant le code suivant dans le fichier 📄`standalone.xml` de votre serveur WildFly (il faut vérifier l'url, le _user_ et le mot de passe) :
        
            ```xml
            <datasource jndi-name="java:/EStoreDB" pool-name="EStoreDB">
                <connection-url>jdbc:mysql://localhost:3306/estore_db</connection-url>
                <driver-class>com.mysql.cj.jdbc.Driver</driver-class>
                <driver>mysql</driver>
                <security>
                    <user-name>root</user-name>
                    <password>admin</password>
                </security>
                <validation>
                    <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"/>
                    <validate-on-match>true</validate-on-match>
                    <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"/>
                </validation>
            </datasource>
            ```

    3. Dans Eclipse, importer le projet **_EStore_**. Pour cela, :
        
        1. Dézipper le projet dans le dossier correspondant à votre Workspace,
        2. faire un clic droit, :material-mouse:`Import > Import...`, puis :material-file-tree:`General > Existing Projects into Workspace`,
        3. sélectionner le dossier correspondant à votre Workspace
        4. et enfin, sélectionner le projet **_EStore_**.

???+success "Drop and create"

    Pour rappel, afin de créer la structure de votre base de données, c'est-à-dire ici les tables, vous devez, dans le fichier 📄`persistence.xml` du projet, indiquer _drop and create_ au premier déploiement du projet (et à chaque modification de votre modèle de données), c'est-à-dire ajouter :
    
    ```xml
    <properties>
        <property name="javax.persistence.schema-generation.database.action" value="drop-and-create"/>
    </properties>
    ```
    
    En faisant cela, à chaque déploiement de votre application, les données sont entièrement supprimées. Vous pouvez donc, au choix :
    
    - Soit supprimer cette option _drop and create_ une fois que la structure a été créée,
    - Soit insérer les données à chaque fois, via le script 📄`datas.sql` fourni à la racine du projet.

???+info "Page d'accueil"

    Vous pouvez accéder au catalogue via cette URL [http://localhost:8080/EStore/](http://localhost:8080/EStore/){:target="_blank"}.

???+info "Détail technique du projet"

    Il s'agit uniquement d'un projet Web Dynamique contenant les couches suivantes :
    
    1. **Le modèle** : Ce sont les classes `Product` et `Comment` disponibles dans le package `edu.polytech.estore.model`, qui font le mapping avec les tables du même nom en BDD.
    2. **Le Dao** : Ce sont les interfaces et classes disponibles dans le package `edu.polytech.estore.dao`.
    
        - `ProductDao` permet d'accéder à la table `Product`.
        - `CommentDao` permet d'accéder à la table `Comment`.
        
    3. **La couche métier** : Ce sont l'interface et la classe présentes dans le package `edu.polytech.estore.business`. L'interface permet d'accéder aux services métiers de l'application.
    4. **La couche contrôleur** : C'est la _Servlet_ présente dans le package `edu.polytech.estore.controller`. Elle permet l'affichage des produits contenus dans le catalogue.
    5. **La vue** : C'est la JSP `products.jsp`, qui affiche le contenu du catalogue.


## 4. Exerice 1

???+exercice "Exercice 1 - Mise en place du Web Service"

    Proposer les requêtes HTTP (ici uniquement les couples _URL_ et _verbe HTTP_) permettant la mise en place des services suivants :
    
    1. Affichage en JSON et en XML de tous les produits présents dans le catalogue.
    2. Affichage en JSON et en XML d'un produit en particulier, identifié via son _id_.
    3. Affichage en JSON et en XML de tous les produits présents dans le catalogue, filtrés sur une catégorie donnée.
    4. Affichage en JSON et en XML de tous les produits présents dans le catalogue, triés par prix croissant ou decroissant, au choix de l'utilisateur (on considère ici qu'on ne peut trier le catalogue, éventuellement filtré sur une catégorie, que par rapport au prix des produits).
    5. Affichage en JSON et en XML de tous les produits présents dans le catalogue, dans la devise choisie par l'utilisateur.
    6. Suppression d'un produit et de tous les commentaires associés, identifié via son _id_.
    7. Création d'un produit, en envoyant toutes les informations en JSON ou en XML.
    8. Modification totale d'un produit, en envoyant les informations en JSON ou en XML.
    9. Modification partielle d'un produit, en envoyant les informations en JSON ou en XML.
    10. Affichage en JSON et en XML de tous les commentaires associés à un produit donné, identifié via l'_id_ du produit.
    
    :warning: À chaque fois où cela est précisé, seuls les formats JSON et XML doivent être acceptés.
    
???+info "Modification totale vs partielle"

    On suppose que l'information suivante est envoyée, pour modifier le produit d'identifiant 3 :
    
    ```json
    {
        "category": "Alimentation",
        "priceInEuro": 30.3
    }
    ```

    1. Modification totale : Le produit 3 est placé dans la catégorie _Alimentation_ et est vendu au prix de 30,30€.
    
        Mais les valeurs des champs `label` et `stock` sont supprimés : ils ont maintenant la valeur `null`.
    
    2. Modification partielle : Le produit 3 est placé dans la catégorie _Alimentation_ et est vendu au prix de 30,30€.
    
        Les champs `label` et `stock` ne changent pas de valeurs, ils gardent celles qu'ils avaient avant la modification.


## 5. Exerice 2

???+exercice "Exercice 2 - Mise en place de ce Web Services"

    Mettre en place tous les Web Services listés dans l'exercice 1, sauf le n°5.

## 6. Exerices 3 & 4

???+exercice "Exercice 3 - Utilisation du Web Service taux d'échange"

    Il faut s'inscrire pour utiliser ce Web Service, afin d'obtenir une clef.
    
    1. Aller ici : [https://apilayer.com/marketplace/exchangerates_data-api](https://apilayer.com/marketplace/exchangerates_data-api){:target="_blank"}
    2. Cliquer sur **_Subscribe for Free_**
    3. Choisi l'offre gratuite.
    4. Se connecter avec votre compte Gmail (de mon côté, la connexion avec GitHub n'a pas fonctionné), ou avec un courriel via Magik Link (vérifier les spams !).
    5. Une fois votre enregistrement confirmé, vous pouvez accéder à votre clef ici : [https://apilayer.com/account](https://apilayer.com/account){:target="_blank"}.
    
    Vous pouvez maintenant accéder au Web Service.
    
    Pour cela, il faut :
    
    - Utiliser le verbe `GET`.
    - Utiliser l'URI `https://api.apilayer.com/exchangerates_data/convert`.
    - Ajouter les paramètres de requêtes suivants :
        
        - `from` : Le code de la devise de départ. La valeur sera toujours `EUR` pour nous.
        - `to` : Le code de la devise d'arrivée. Par exemple `USD` ou `GBP` si on souhaite convertir le montant en dollar ou en livre.
        - `amount` : Le montant (dans la devise de départ) à convertir dans la devise d'arrivée.
        
    - Dans l'en-tête (_header_) de la requête, ajouter la clef `apikey` avec comme valeur la clef que vous avez obtenue.

???+exercice "Exercice 4 - Création du client"

    Implementer le Web Service n°5 décrit dans l'exercice 1.
    
    Le résultat renvoyé par le Web Service externe décrit dans l'exercice 3 est une nouvelle source de données.

???+success "Affichage du montant dans une autre devise"

    Le montant calculé (pour affichage dans une autre devise) va être stocké dans le champ `priceInCurrency` du _bean_ `Product`.
    
    Pour que celui-ci soir sérialisé, et donc affiché lors de l'export en JSON ou en XML, il faut supprimer l'annotation `@XmlTransient` qu'il possède.
    
???+info "Remarques sur la création du client"

    Nous avons vu dans le TP précédent comment créer un client pour consommer un Web Service externe, depuis une application Java. Pour rappel, il faut :
    
    1. Créer un objet `Client`.
    2. À partir de celui-ci, créer un objet `WebTarget` en indiquant l'URI.
    3. Sur cet objet `WebTarget`, ajouter le "path", et les éventuels paramètres de requête.
    4. À partir de l'objet `WebTarget`, via la méthode `request`, créer un objet `Builder`.
    5. Sur cet objet `Builder`, il est possible d'ajouter des informations dans l'en-tête via la méthode `header`.
    6. Enfin, via cet objet `Builder`, on appelle la méthode correspondant au verbe HTTP souhaité.
    
    La seule chose nouvelle ici par rapport au TP4 est l'ajout d'information dans l'en-tête de la requête HTTP (votre clef).