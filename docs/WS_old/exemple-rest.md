# Exemples REST

1. Météo à Tours (il faut une clé) :
    [http://api.openweathermap.org/data/2.5/weather?q=tours,fr&APPID=16a4018534d362c96ea5b9e987b32259](http://api.openweathermap.org/data/2.5/weather?q=tours,fr&APPID=16a4018534d362c96ea5b9e987b32259)

2. Recherche par adresse :
    [https://api-adresse.data.gouv.fr/search/?q=64%20avenue%20jean%20portalis&limit=15](https://api-adresse.data.gouv.fr/search/?q=64%20avenue%20jean%20portalis&limit=15)

3. À partir des coord récupérées juste au-dessus :
    [http://api.openweathermap.org/data/2.5/weather?lon=0.684399&lat=47.364675&APPID=16a4018534d362c96ea5b9e987b32259](http://api.openweathermap.org/data/2.5/weather?lon=0.684399&lat=47.364675&APPID=16a4018534d362c96ea5b9e987b32259)
