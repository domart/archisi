---
author: Benoît Domart
title: Exercice 1 - Tester un WS
---

# Exercice 1 - Tester un WS avec SoapUI

Pour tester un Web Service, nous allons utiliser l'outil [**SoapUI**](https://www.soapui.org/downloads/latest-release/){target=_blank}.
Pour exécuter un Web Service, il suffit ensuite d'avoir son WSDL. Comme indiqué plus tôt, il suffit de connaître l'URL du service, et de rajouter `?wsdl` à la fin de l'URL.

Voici quelques Web Services qu'il faut tester :

- Convertisseur Celcius ↔️ Farenheit
    ``` title=""
    https://www.w3schools.com/xml/tempconvert.asmx?WSDL
    ```
- De nombreuses informations sur le pays demandé !
    ``` title=""
    http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL
    ```

Ces deux exemples viennent de [ce site](https://www.numpyninja.com/post/save-time-compiled-list-of-free-wsdl-urls){target=_blank}, qui en contient 3 autres.
<!--https://ngrashia.medium.com/compiled-list-of-free-and-working-wsdl-urls-a7a54be6b91-->


Pour les tester, il faut lancer SoapUI, puis créer un projet SOAP et enfin indiquer le lien vers le WSDL. Par exemple :

<center>
    ![Exemple SoapUI](../gif/soap_exemple.gif)
</center>