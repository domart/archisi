---
author: Benoît Domart
title: Exercice 2 - Création du projet client (lourd)
---

# Exercice 2 - Création du projet client (lourd)

Nous allons maintenant implémenter la brique "Appli Java" du schéma suivant :

<figure markdown>
  ![archi appli](../images/schema_archi_V3.png)
  <figcaption>Architecture de notre application</figcaption>
</figure>

## 1. Complétion du projet EJB

???+exercice "Complétion du projet EJB"

    Nous l'avons vu dans le cours, un EJB est en fait un triplet :

    1. Une interface *local*, pour les appels de l'EJB depuis la même JVM, ce que nous avons toujours fait pour l'instant.
    2. Une interface *remote*, pour les apples distants, depuis une autre JVM donc.
    3. Une classe implémentant ces deux interfaces.

    Il y a donc deux modifications à effectuer dans notre projet EJB :

    1. La création de l'interface *remote*, au même niveau que l'interface *local*, et qui a exactement le même contenu :

        ```java title="☕ Code Java - Interface NoteBusinessRemote"
        @Remote
        public interface NoteBusinessRemote {
            public List<ResultBean> getResultsList();

            public void insertNote(NoteBean note);

            public void insertStudent(StudentBean studentBean);

            public void updateResult(ResultBean result);

            public Float computeMean(List<ResultBean> notesList);
        }
        ```

        Attention à ne pas oublier l'annotation `@Remote` !

        ???+success "Meilleure pratique"

            Comme le contenu des deux interfaces est **exactement** le même, il serait préférable de créer une interface contenant les signatures, et de faire hériter les deux interfaces locale et distante.

    2. Dans la classe `NoteBusinessImpl`, ajouter l'implémentation de cette nouvelle interface :

        ```java title="☕ Code Java - Classe NoteBusinessImpl"
        public class NoteBusinessImpl implements NoteBusiness, NoteBusinessRemote {
        ```

## 2. Création du projet Java

???+exercice "Création du projet java"

    1. Créer un projet *Maven* avec l'archetype `maven-archetype-quickstart`, avec `gestionnotesclient` comme nom (et donc comme `artefactId`).

    2. Dans le 📄`pom.xml`, ajouter la dépendance vers le projet EJB (à nouveau, il suffit en fait d'avoir les *beans* et l'interface *remote* que nous venons de créer) :

        ```xml title="pom.xml - Projet gestionnotesclient"
        <dependency>
            <groupId>fr.univtours.polytech</groupId>
            <artifactId>gestionnotesejb</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
        ```
    
    3. Toujours dans ce même  📄`pom.xml`, ajouter la dépendance vers la bibliothèque permettant d'appeler des EJBs sur un serveur WildFly :

        ```xml title="pom.xml - Projet gestionnotesclient"
        <dependency>
            <groupId>org.wildfly</groupId>
            <artifactId>wildfly-ejb-client-bom</artifactId>
            <version>31.0.1.Final</version>
            <type>pom</type>
        </dependency>
        ```

        ???+success "Version"

            Pensez à vérifier qu'il s'agit bien de la dernière version. Il suffit de chercher cette bibliothèque dans le référentiel *Maven*.
    
    4. Ce projet va uniquement contenir une seule classe, la classe `App.java` créée par défaut, qui contient une méthode `main`.

        Il va falloir appeler l'EJB. Mais ici, nous ne sommes pas dans le même projet, nous ne pouvons donc pas utiliser l'injection de dépendances ...

        Il faut donc appeler l'EJB *à la main*, c'est-à-dire en faisant un `lookup` à partir de son nom JNDI.

        On trouve ce nom JNDI en cherchant la chaîne de caractères commençant par `ejb:` dans les logs du serveur WildFLy. Il y a un seule EJB, donc il doit y avoir une seule ligne correspondant, qui doit ressembler à ceci :

        ``` title="📋 Console WildFly"
        ejb:gestionnotesear/gestionnotesweb-1.0-SNAPSHOT/NoteBusinessImpl!fr.univtours.polytech.gestionnotesejb.business.NoteBusinessRemote
        ```

        1. Dans cette classe `App.java`, créer la méthode `getNoteBusiness` qui va renvoyer l'EJB (concrètement, elle renvoie l'interface *remote*) :

            ```java
            /**
             * 
             * @return L'EJB session pour manipulation.
             * @throws NamingException Si l'EJB n'est pas trouvé dans l'annuaire JNDI.
             */
            private static NoteBusinessRemote getNoteBusiness() throws NamingException {
                Properties jndiProperties = new Properties();
                jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
                jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");
                Context ctx = new InitialContext(jndiProperties);

                String url = "ejb:gestionnotesear/gestionnotesweb-1.0-SNAPSHOT/NoteBusinessImpl!fr.univtours.polytech.gestionnotesejb.business.NoteBusinessRemote";

                System.out.println("URL : " + url);

                NoteBusinessRemote business = (NoteBusinessRemote) ctx.lookup(url);

                return business;
            }
            ```
        
        2. Enfin, dans la méthode `main`, nous allons appeler cette méthode et insérer deux étudiants en BDD :

            ```java
            public static void main(String[] args) throws NamingException {
                // Cette objet business va nous permettre d'appeler les services métiers
                // disponibles,
                // c'est-à-dire ceux définis dans l'interface distante : NoteBusinessRemote.
                NoteBusinessRemote noteBusiness = getNoteBusiness();

                StudentBean studentA = new StudentBean();
                studentA.setFirstName("Alice");
                studentA.setName("A");
                StudentBean studentB = new StudentBean();
                studentB.setFirstName("Bob");
                studentB.setName("B");

                noteBusiness.insertStudent(studentA);
                noteBusiness.insertStudent(studentB);
            }
            ```
    
    5. Il ne reste plus qu'à exécuter la méthode `main` de ce projet (c'est un projet Java *classique*, pas un projet JEE), et de vérifier que les étudiants *Alice A* et *Bob B* ont bien été ajoutés en BDD !

???+failure "ClassNotFoundException"

    Si tu obtiens l'erreur suivante :

    ``` title="📋 Logs du projet ClientJava (main de la classe Client)"
    java.lang.ClassNotFoundException: org.wildfly.naming.client.WildFlyInitialContextFactory
    ```

    c'est que tu n'as pas bien ajouté la dépendance vers la bibliothèque `wildfly-ejb-client-bom`.