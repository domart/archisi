---
author: Benoît Domart
title: Exercice 1 - Mise en place du nouveau projet
---

# Exercice 1 - Mise en place du nouveau projet

Nous avons donc 3 projets Java à créer. Ces trois projets étant liés, nous allons créer un 4ème projet qui va les *encapsuler*.

Ce projet *encapsulant* devrait en toute logique s'appeler ***gestionnotes***, mais ce nom de projet étant déjà pris, et comme il s'agit d'un projet d'entreprise (EAR signifie **Entreprise ARchive**), nous allons l'appelle ***gestionnotesentreprise***.

???+note "Les archives Java"

    Pour être déployés, les projets Java doivent être mis sous forme d'archives. Il existe principalement trois types d'archives, qui sont en fait **toutes** des fichiers ZIP :

    1. Les **_JAR_**, pour **_Java ARchive_** : ils contiennent des classes Java et des métadonnées. Ils permettent de déployer des projets Java classiques ou des projets _EJB_.
    2. Les **_WAR_**, pour **_Web application ARchive_** : ils contiennent des classes Java, des métadonnées, et tout le contenu Web (HTML, CSS, JavaScript, images, ...). Ils permettent de déployer des projets _Web_.
    3. Les **_EAR_**, pour **_Entreprise Application aRchive_** : ils contiennent plusieurs modules (au moins un). Il peut y avoir un ou plusieurs modules _Web_, un ou plusieurs modules _EJB_ et un ou plusieurs modules Java.

## 1. Création du projet englobant

???+exercice "Création du projet englobant"

    1. Créer un projet *Maven* classique, c'est-à-dire en choisissant l'archetype `maven-archetype-quickstart`, avec `gestionnotesentreprise` comme nom (et donc comme `artefactId`).

        ???+success "Projets Maven"

            Ce ne sera pas toujours précisé par la suite, mais il faudra **toujours** indiquer `fr.univtours.polytech` comme `groupeId`.

            De même, lorsqu'on choisit un archetype pour créer un projet *Maven*, on choisira toujours les dernières versions lorsque plusieurs versions sont proposées.
        
        ???+note "Choix de l'archetype"

            Lorsque tu crées un projet Maven, et que tu sélectionnes l'archetype `maven-archetype-quickstart` (et que tu as cliqué sur *more*), il y a plusieurs choix possible.

            Il faut alors sélectionner celui de `org.apache.maven.archetypes`.
    
    2. Dans le 📄`pom.xml` de fichier :

        1. Supprimer toute la partie `<dependencies>`.
        2. Supprimer toute la partie `<build>`.
        3. Dans la partie `<properties>`, supprimer tout sauf la ligne 
        
            ```xml title="pom.xml"
            <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
            ```

        4. Enfin, changer le ***packaging*** (ou ajoute-le s'il n'existe pas) en indiquant :

            ```xml
            <packaging>pom</packaging>
            ```
        
    3. Supprimer les dossiers 📂`src` et 📂`test`.

???+warning

    Attention, tous les projets qui vont être créés maintenant seront **toujours** des sous-projets de celui que nous venons de créer.

## 2. Création du projet EAR

???+exercice "Création du projet EAR"

    1. Créer un projet *Maven* classique, c'est-à-dire en choisissant l'archetype `maven-archetype-quickstart`, avec `gestionnotesear` comme nom (et donc comme `artefactId`).
    2. Dans le 📄`pom.xml` de fichier :

        1. Supprimer toute la partie `<dependencies>`.
        2. Supprimer toute la partie `<build>`.
        3. Dans la partie `<properties>`, supprimer tout sauf la ligne 
        
            ```xml title="pom.xml"
            <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
            ```

        4. Changer le ***packaging*** (ou ajoute-le s'il n'existe pas) en indiquant :

            ```xml title="pom.xml - gestionnotesear"
            <packaging>ear</packaging>
            ```
        
        5. Enfin, ajouter le plugin qui permettra de créer l'EAR, en précisant le *context root* de notre future application :

            ```xml title="pom.xml - gestionnotesear"
            <build>
                <finalName>gestionnotesear</finalName><!-- (1)!-->
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-ear-plugin</artifactId>
                        <contextRoot>/gestionnotesweb</contextRoot><!-- (2)!-->
                    </plugin>
                </plugins>
            </build>
            <build>
                <finalName>gestionnotesear</finalName>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-ear-plugin</artifactId>
                        <configuration>
                        <modules>
                            <webModule>
                                <groupId>fr.univtours.polytech</groupId>
                                <artifactId>gestionnotesweb</artifactId>
                                <contextRoot>/gestionnotesweb</contextRoot>
                            </webModule>
                        </modules>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
            ```

            1. C'est le nom de l'EAR qui sera créé : `gestionnotesear.ear`.
            2. Le *context root* d'une application Web est comme son nom l'indique sa racine.

                Pour l'instant, nous n'avions qu'un projet Web donc le *context root* était son nom.

                Il y a maintenant plusieurs projets, il faut donc le spécifier.

                Cela signifie que **toutes** les URLs de notre application commenceront par `http://localhost:8080/gestionnotesweb`.
        
    3. Supprimer le dossier 📂`test`.

## 3. Création du projet EJB

???+exercice "Création du projet EJB"

    1. Créer un projet *Maven* classique, c'est-à-dire en choisissant l'archetype `maven-archetype-quickstart`, avec `gestionnotesejb` comme nom (et donc comme `artefactId`).
    2. Dans le 📄`pom.xml` de fichier :

        1. Supprimer toute la partie `<dependencies>`.
        2. Supprimer toute la partie `<build>`.
        3. Dans la partie `<properties>`, supprimer tout sauf la ligne 
        
            ```xml title="pom.xml"
            <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
            ```

        4. Toujours dans la partie `<properties>`, ajouter :

            ```xml title="pom.xml - gestionnotesejb"
            <maven.compiler.source>11</maven.compiler.source>
		    <maven.compiler.target>11</maven.compiler.target>
            ```

        4. Changer le ***packaging*** (ou ajoute-le s'il n'existe pas) en indiquant :

            ```xml title="pom.xml - gestionnotesejb"
            <packaging>jar</packaging>
            ```
        
        4. Ajouter les bibliothèque permettant de développer une application JEE (avec sa version 10, ce que nous faisons depuis le début de ce cours) :

            ```xml title="pom.xml - gestionnotesejb"
            <dependencies>
                <dependency>
                    <groupId>jakarta.platform</groupId>
                    <artifactId>jakarta.jakartaee-api</artifactId>
                    <version>10.0.0</version>
                    <scope>provided</scope>
                </dependency>
            </dependencies>
            ```
        
        5. Enfin, ajouter le plugin qui permettra de créer le JAR :

            ```xml title="pom.xml - gestionnotesejb"
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-ejb-plugin</artifactId>
                    </plugin>
                </plugins>
            </build>
            ```
        
    3. Supprimer le dossier 📂`test`.

## 3. Création du projet WEB

???+exercice "Création du projet WEB"

    Créer un projet ***gestionnotesweb***, en utilisant cette fois-ci l'archetype `webapp-jakartaee10`.

## 4. Ajout des dépendances entre les projets

???+exercice "Ajout des dépendances entre les projets"

    Il faut maintenant ajouter les dépendances entre les projets :

    1. Dans le projet Web, il faut pouvoir utiliser les *beans* ainsi que les interfaces de la couche métier.

        Pour simplifier cela, on créé une dépendance entre le projet Web et le projet EJB.

        ???+success "Dans un *vrai* projet"

            Un meilleur usage serait de mettre les *beans* et les interfaces de la couche métier dans un autre projet, communs aux couches présentation et métier.

            Ce projet s'appelle souvent ***gestionnotescommon***, et est une dépendance des deux projets ***gestionnotesweb*** et ***gestionnotesejb***.
    
    2. Dans le projet EAR, il faut ajouter des dépendances vers les deux modules de l'application :

        1. Le projet Web.
        2. Le projet EJB.
    
    Pour cela, il faut modifier les 📄`pom.xml` de la façon suivante :

    1. Dans le 📄`pom.xml` du projet ***gestionnotesweb***, dans la balise `<dependencies>`, ajouter :

        ```xml title="pom.xml - gestionnotesweb"
        <dependency>
		    <groupId>fr.univtours.polytech</groupId>
		    <artifactId>gestionnotesejb</artifactId>
		    <version>1.0-SNAPSHOT</version>
		</dependency>
        ```
    
    2. Dans le 📄`pom.xml` du projet ***gestionnotesear***, dans la balise `<dependencies>`, ajouter :

        ```xml title="pom.xml - gestionnotesear"
        <dependency>
		    <groupId>fr.univtours.polytech</groupId>
		    <artifactId>gestionnotesejb</artifactId>
		    <version>1.0-SNAPSHOT</version>
		</dependency>
        <dependency>
	        <groupId>fr.univtours.polytech</groupId>
	        <artifactId>gestionnotesweb</artifactId>
	        <version>1.0-SNAPSHOT</version>
	        <type>war</type>
	    </dependency>
        ```

## 5. Implémentation du projet

???+exercice "Implémentation du projet"

    Il nous reste enfin à récupérer les sources ! Il suffit pour cela de copier les sources du projet ***gestionnotes*** au bon endroit :

    1. Dans le projet ***gestionotesejb*** :
    
        1. Copier les packages `model`, `dao` et `business`.
        2. Copier le fichier 📄`resources/META-INF/persistence.xml`.

    2. Dans le projet ***gestionnotesweb*** :

        1. Copier le package `controller`.
        2. Copier le dossier 📂`webapp` (plus précisément les JSP 📄`notesList.jsp` et 📄`addNote.jsp` - et les autres éventuellement créées)
    
    ???+success "Copier un package"

        Attention ! Lorsque tu vas copier des packages, les noms de ces packages dans les classes Java ne seront plus corrects !

        Il faudra en effet transformer les `fr.univtours.polytech.gestionnotes` en `fr.univtours.polytech.gestionnotesejb` ou en `fr.univtours.polytech.gestionnotesweb` selon la classe.

## 6. Test

Il n'y a maintenant plus qu'à tester tout cela !