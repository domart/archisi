# TD 7 - Mise en place des _EJBs_

L'bjectif de ce TD est de séparer la couche applicative (`G - T - A` sur le serveur Web dans le schéma ci-dessous) en 2 parties - "présentation" et "métier" - et voir comment établir une communication entre ces deux couches.

Concrétement, nous allons mettre en place une architecture de ce type :
<figure markdown>
  ![archi td7](../images/archi-4tiers.png)
  <figcaption>Architecture 4-tiers</figcaption>
</figure>