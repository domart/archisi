---
author: Benoît Domart
title: Présentation du TD
---

# Présentation du TD

Dans les TD5 et TD6, nous avons vu comment mettre en place une architecture 3 tiers de ce type :

<figure markdown>
  ![archi 3-tiers](../images/archi-3tiers.png)
  <figcaption>Architecture 3-tiers (TD5 & TD6)</figcaption>
</figure>

Notre application est fonctionnelle, mais comme nous l'avions indiqué au tout début de ce cours, il faut toujours penser en terme de réutilisabilité. On peut imaginer qu'une autre application Web souhaite également accéder aux mêmes services métiers. De même, on pourrait souhaiter qu'un client lourd (une application Java "classique" par exemple) puisse également le faire.

De même que les _Servlets_ permettent à un navigateur Web de se connecter à l'application JEE, il existe un autre objet JEE permettant à une application Java de se connecter à des services métiers à distance. Ce sont les **_EJB_**, pour _Entreprise JavaBeans_.

Ils vont nous permettre de mettre en place une architecture 3-tiers ou 4-tiers de ce type (les couches "présentation" et "métier" pourront toujours être sur le même serveur, comme précédemment, ou, ce qui est nouveaux, **sur des serveurs différents**) :

<figure markdown>
  ![archi td4](../images/archi-4tiers.png)
  <figcaption>Architecture 4-tiers</figcaption>
</figure>

Cela permet à différentes applications Web et différents clients lourds Java de se connecter aux services métiers proposés :

<figure markdown>
  ![archi td4 avec plusieurs applications clientes](../images/archi-4tiers-2.png)
  <figcaption>Architecture 4-tiers avec plusieurs applications clientes</figcaption>
</figure>

???+ note "3 projets Java"

    Pour pouvoir définir des _EJBs_ (sessions), il faut un projet Java _EJB_. Notre projet ne sera donc plus implémenté dans un seul projet Web dynamique, mais il va nous falloir trois projets :

    1. Un projet **Web dynamique** pour la couche présentation.<br>
        Un tel projet est déployé sur un serveur via un WAR (pour _Web Application Archive_).
    2. Une projet **EJB** pour la couche métier.<br>
        Un tel projet est déployé sur un serveur via un JAR (pour _Java Archive_).
    3. Une projet **application d'entreprise** qui va contenir ces deux modules.<br>
        Un tel projet est déployé sur un serveur via un EAR (pour _Entreprise Application Archive_).<br>
        **Sur le serveur Wildfly, nous ne déploierons en fait que ce projet.**<br>
        <br>
        Il va en fait même nous falloir un 4^e^ projet :
    4. Un projet **java** classique, pour représenter le client lourd.<br>
        Ce projet ne sera pas déployé sur le serveur.

Nous allons maintenant implémenter cette architecture 4-tiers, plus précisément, nous allons implémenter le schéma suivant :

<figure markdown>
  ![Architecture TD4](../images/schema_archi_V3.png)
  <figcaption>Schéma de l'architechture du TD7 (4-tiers)</figcaption>
</figure>
