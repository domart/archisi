---
author: Benoît Domart
title: Exercice 3 - Utilisation de la session
tags:
    - 0-simple
---

# Exercice 3 - Utilisation de la session

???+success "La session en JEE"

    Problématique : HTTP est un protocole sans état". Aucune information n'est conservée entre deux requêtes HTTP.

    On souhaite avoir un objet "session" qui conserve en mémoire des informations liées à l'utilisateur. On souhaite par exemple pouvoir retenir son nom, qu'il a saisi lors de la connexion à l'application, sans avoir à lui redemander à chaque page.

    JEE permet de faire cela grace à l'objet `jakarta.servlet.http.HttpSession`. Cet objet utilise en fait un cookie pour faire le lien entre les différentes requêtes HTTP, mais ceci est transparent pour le développeur qui l'utilise.

    Pour obtenir cet objet, on passe par la requête, en effectuant `request.getSession()`. Cela permet d'obtenir la session de l'utilisateur, et éventuellement de la créer si elle n'existe pas encore.

    De la même manière qu'avec l'objet `jakarta.servlet.http.HttpServletRequest`, il est ensuite possible :

    - de sauvegarder des informations, via `void HttpSession.setAttribute(String name, Object value)`,
    - de les récupérer via `Object getAttribute(String name)`.

## Exercice 3.1 - Compteur de passage

???+exercice "Compteur de passage"

    Créer une Servlet `CounterServlet` qui affiche un compteur qui est incrémenté à chaque accès sur cette servlet durant la navigation de l'utilisateur.

    Il faut vérifier que l'utilisateur peut afficher d'autres pages (c'est-à-dire exécuter d'autres servlets), et que le compteur continue de s'incrémenter lorsqu'il exécute à nouveau cette Servlet.

    L'affichage de ce compteur incrémenté se fera via [http://localhost:8080/td3/counter](http://localhost:8080/td3/counter){target=_blank}.

    ???+warning "Attention"

        La Servlet doit récupérer et incrémenter la valeur du compteur, mais elle doit également rediriger vers une JSP qui va afficher cette valeur.

        Encore une fois, on n'écrit jamais de code HTML dans une Servlet !

## Exercice 3.2 - Connexion à l'application

???+exercice "Connexion à l'application"

    Modifier la méthode `doPost` de la *servlet* de connexion (de l'exercice précédent) pour que le prénom de l'utilisateur ne soit pas stocké en requête, mais en session afin de pouvoir être utilisé dans n'importe quelle page.

    Dans chaque JSP de ce projet, afficher le prénom de l'utilisateur connecté (*null* si personne ne s'est connecté), ce prénom étant cliquable. Le clic permettant à l'utilisateur de se déconnecter.