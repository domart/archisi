---
author: Benoît Domart
title: Exercice 2 - Salut !
tags:
    - 0-simple
---

# Exercice 2 - Salut !

Reprendre l'exercice 2 du TD précédent ([Cf. ici](../../TD2/exercice2){target=_blank}).

Modifier la méthode `doPost` de cette Servlet, pour **qu'elle redirige vers une JSP** affichant le texte suivant :

`Salut <prénom_indiqué>`

où `<prénom_indiqué>` est le prénom indiqué par l'utilisateur dans la page précédente :

<center>
    ![Exemple](./exemple_prenom.gif)
</center>

???+note "Remarque"

    On a fait ce qui était indiqué dans le bloc "Attention, on ne fera en fait pas ça en pratique !" de l'exercice 2 du TD précédent ...

???+success "Encodage"

    Dans les JSP, pour ne pas avoir de problème d'encodage, en plus de ce qui a été fait précédemment, il faut ajouter, à la première ligne :

    ```JSP
    <%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8"%>

    ```