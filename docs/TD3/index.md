# TD3 - Les JSP 📄

L'objectif de ce TD est de se familiariser avec les JSP, et de découvrir une nouvelle notion liée aux Servlet : la session.

Concrétement, nous allons mettre en place une architecture de ce type :
<center>![archi td3](../images/archi-2tiers-4.png)</center>



???+success "Servlet et code HTML"

    À partir de maintenant on utilisera toujours une Servlet pour afficher du contenu Web, c'est-à-dire une page HTML ou une page JSP.

    De même, on n'écrira jamais du code HTML dans une Servlet. La Servlet effectue des traitements (éventuellement) et redirige ensuite vers le contenu web sélectionné.