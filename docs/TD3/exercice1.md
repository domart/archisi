---
author: Benoît Domart
title: Exercice 1 - Une première JSP
tags:
  - 0-simple
---

# Exercice 1 - Une première _JSP_

Le but de cet exercice est de créer une première JSP, qui sera accessible via une _Servlet_.

Nous allons créer un formulaire, contenant un seul champ de type _nombre_ (`<input type="number"/>`), qui permet, lorsqu'on clique sur <bouton>Valider</bouton> d'afficher "_Bonjour !_" autant de fois que la valeur de ce champ :

<center>
    ![](./exercice1.gif){width=70%}
</center>

1. Comme d'habitude, on commence par créer un projet, qu'on appellera `td3`, dans le *groupid* `fr.univtours.polytech`.
1. Dans le package `fr.univtours.polytech.td3.servlet`, créer une _Servlet_ `ListeServlet`, dont la méthode `doGet` créé une liste de `String` vide, placée dans la requête avec la clef `LISTE`, et qui redirige vers `afficheListe.jsp`
    ``` java linenums="1" title="☕ Code Java - méthode doGet" 
    request.setAttribute("LISTE", new ArrayList<String>()); //(1)!
    request.getRequestDispatcher("afficheListe.jsp").forward(request, response);
    ```

    1. Dans la _JSP_ `afficheListe.jsp`, nous allons utiliser une liste. Pour ne pas avoir d'erreur (une `NullPointerException`), on place dans la requête une liste vide au premier affichage.

    ???+ note "Paramètres et Attributs en requête"

        On peut placer **deux types d'objets** dans la requête :

        1. Des **paramètres** :
        
            Depuis un formulaire, on peut placer des `parameter`. Ces sont des `java.lang.String`. Il n'est pas possible de placer un paramètre en requête via l'objet Java (on ne peut pas faire `request.setParameter(clef, valeur)`).

            **Les paramètres sont envoyés par le client au serveur.**
        
        2. Des **attributs** :
        
            Dynamiquement, dans une _Servlet_ par exemple, on peut placer des `attributes`. Ces sont des `java.lang.Objects`, qu'il faut donc _caster_ lorsqu'on les manipule. On place des attributs en requête via l'objet Java : `request.setAttribute(clef, valeur)`.

            **Les attributs sont envoyés par le serveur (depuis une *Servlet* la plupart du temps) à lui-même (une JSP la plupart du temps, ou une autre *Servlet*).**

2. Déployer cette _Servlet_ , en lui donnant (par exemple) `listeServlet` comme nom et `/liste` comme URL permettant de l'exécuter.
3. Créer la page _JSP_ `afficheListe.jsp`, qui contient le formulaire avec le champ nombre, dont le `name` est `taille` (c'est le nom du paramètre en requête).

    Placer la _JSP_ dans le dossier 📂`src/main/webapp`.
    ``` jsp linenums="1"
    <%@page import="java.util.List"%><!--(1)!-->
    <html>
    <head>
        <title>Affiche une liste dynamique</title>
        <meta charset="UTF-8"/>
    </head>
    <body>
        <form action="liste" method="post">
            <input type="number" name="taille"
                value="<%=((List<String>)request.getAttribute("LISTE")).size()%>"/><!--(2)!-->
            <input type="submit" name="Valider"/>
        </form>
        <table>
            <%
            List<String> liste = (List<String>)request.getAttribute("LISTE"); //(3)!
            for (String mot : liste) {
            %>
                <tr><td><%=mot %></td></tr>
            <% } %>
        </table>
    </body>
    </html>
    ```

    1. Il s'agit d'une directive JSP.

        Elle permet d'importer un objet Java nécessaire.

    2. Il s'agit d'une expression JSP.
    
        L'attribut `value` permet de donner une valeur à ce champ à l'affichage de la page. Ici, la valeur sera 0 au premier affichage (la taille de la liste vide), puis celle saisie précédemment aux suivants.
        
    3. Il s'agit d'un scriptlet.
        
        On récupère la liste placée en attribut de la requête, et on affiche autant de ligne dans le tableau qu'il y a d'éléments dans la liste.
    
    ???+question "Où placer les _JSP_ ?"

        Les JSP (ainsi que les pages HTML, le javascript, le CSS, ..., bref, tout le contenu Web) doivent être placées dans le dossier **contenu web**.
        
        Par défaut, c'est le dossier 📂`src/main/webapp` :

4. Enfin, dans la _Servlet_ ajouter la méthode `doPost`, qui gère le clic de l'utilisateur sur <bouton>Valider</bouton> :
    ``` java linenums="1" title="☕ Code Java - méthode doPost"
    Integer taille = Integer.parseInt(request.getParameter("taille"));
    List<String> liste = new ArrayList<String>();
    for (int i = 0; i < taille; i++) {
        liste.add("Bonjour !"); //(1)!
    }
    request.setAttribute("LISTE", liste); //(2)!
    request.getRequestDispatcher("afficheListe.jsp").forward(request, response); //(3)!
    ```

    1. On génère la liste avec autant de _Bonjour !_ que souhaité.
    2. On place cette liste en attribut de la requête, pour pouvoir la récupérer dans la _JSP_.
    3. On redirige la requête vers la _JSP_.

5. Vérifier que l'application est fonctionnelle via [http://localhost:8080/td3/liste](http://localhost:8080/td3/liste){target=_blank}.
