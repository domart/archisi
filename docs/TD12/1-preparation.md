---
author: Benoît Domart
title: Préparation
---

# TP noté - Préparation


???+tip "À faire avant vendredi 13/12"
    
    - Préparer une instance sur votre BDD s'appelant `tp_note_2`.
    - Depuis la console d'administration WildFly, créer la datasource `java:/TpNote2`. Pour rappel, voici ce qu'il faut faire :

        1. Sélectionner "MySQL"
        2. Indiquer `TpNote2` comme *Name*, et `java:/TpNote2` comme *JNDI Name*.
        3. Ne rien changer.
        4. Indiquer `jdbc:mysql://localhost:3306/tp_note_2` dans Connection URL (il faut adapter cette URL si le port est différent, et si le schéma créé sur la BDD n'est pas gestion_notes), et saisir le *User Name* et l'éventuel *Password*.
        5. Tester la connexion.
        6. Valider la création de la *datasource*.
    
    Vous pouvez également directement copier (**en l'adaptant**) le code suivant :

    ```xml title="📄standalone.xml"
    <datasource jndi-name="java:/TpNote2" pool-name="TpNote2">
        <connection-url>jdbc:mysql://localhost:3306/tp_note_2</connection-url>
        <driver-class>com.mysql.cj.jdbc.Driver</driver-class>
        <driver>mysql</driver>
        <security user-name="root" password="pwd"/><!--(1)!-->
        <validation>
            <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"/>
            <validate-on-match>true</validate-on-match>
            <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"/>
        </validation>
    </datasource>
    ```

    1. Si vous n'avez pas de mot de passe pour vous connecter, il faut supprimer l'attribut `password`, c'est-à-dire indiquer

        ```xml title=""
        <security user-name="root"/>
        ```