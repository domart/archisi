# TD12 - TP noté REST


## À faire avant de commencer

???success "Correction des TD10 et TD11"

    Vous trouverez ici le projet Web correction des TD10 et TD11 :

    <center>
        [Correction location (TD10 et TD11)](https://scm.univ-tours.fr/domart/location_correction){ .md-button target=_blank }
    </center>

???+success "À faire avant de commencer"

    Aller voir [ici](./1-preparation.md).