---
author: Benoît Domart
title: TP noté
---

# TP noté

???+success "Objectif"

    L'objectif est de mettre en place une application contenant une liste de film qui permette des les noter (avec une note entre 1 et 5).

    Notre application conservera uniquement le titre du film, ainsi que sa note.

    D'autres informations seront affichées (l'année de sortie, les acteurs principaux et l'affiche du film), mais celle-ci ne seront pas stockées dans notre base de données, elles seront récupérées depuis l'API REST non officielle de IMDB.

    Notre application exposera de plus tous ses services, en tant que Web Service REST.

Les exercices sont séparés dans la présentation du TP, mais ils peuvent bien sûr être traités dans l'ordre souhaité.

## 1. Création de l'application

???+exercice "Exercice 1 - Création de l'application"

    Commencez par créer une appli Java Web nommée `tp2note` (ou avec un autre nom, ce n'est pas très grave). Pour rappel, vous avez des infos [ici](../../Aides/06-gestionProjet/){target=_blank} sur comment faire, si besoin.

    Voici notre modèle de données :

    <center>
        <table>
            <tr>
                <td>
                    ```mermaid
                    classDiagram
                        MOVIE
                        class MOVIE{
                            -ID: Integer
                            -TITLE: Varchar
                            -NOTE: Integer
                        }
                    ```
                </td>
            </tr>
        </table>
    </center>

    Notre application va permettre d'afficher la liste des films présents dans la base de données sous la forme d'un tableau.

    Sur chaque ligne de ce tableau, il y aura deux boutons (ou liens) :

    1. **-** : Permet de diminuer la note donnée à ce film.
    1. **+** : Permet d'augmenter la note donnée à ce film.

    Entre ces deux boutons (ou liens), la note est affichée.

    Voici les règles de gestion à mettre en place :

    - **RG1 : Clic sur + sur un film qui n'a pas encore de note.**

        La note affichée est 1.
    
    - **RG2 : Clic sur + sur un film qui a déjà une note.**

        Si la note est strictement inférieure à 5, celle-ci est incrémentée. Sinon, elle n'est pas modifiée (rien n'est notifié à l'utilisateur, il constate que la note n'est pas modifiée).

    - **RG3 : Clic sur - sur un film qui n'a pas encore de note.**

        La note affichée est 5.
    
    - **RG4 : Clic sur - sur un film qui a déjà une note.**

        Si la note est strictement supérieure à 1, celle-ci est décrémentée. Sinon, elle n'est pas modifiée (rien n'est notifié à l'utilisateur, il constate que la note n'est pas modifiée).


    ???+example "BONUS"
    
        Ajouter l'écran permettant la création d'un film dans la BDD, en permettant à l'utilisateur d'ajouter le titre et une note éventuelle.

    ???+note "Remarque"

        D'autres informations seront affichées sur cette page dans l'exercice 2 (les acteurs principaux, l'année de sortie et l'affiche du film).

## 2. Consommation d'une API REST

???+exercice "Exercice 2 - Consommation d'une API REST"

    Nous allons utiliser l'[API non officielle de IMDB](https://imdb.iamidiotareyoutoo.com/docs/index.html){target=_blank}. La recherche d'information sur un film donné s'effectue simplement grace à une URL du type :

    ``` title=""
    https://imdb.iamidiotareyoutoo.com/search?q=...
    ```

    en remplaçant les `...` par le titre du film en question.

    Utiliser cette API pour afficher les informations supplémentaires sur la page d'accueil de notre application, à savoir l'année de sortie, les acteurs principaux et l'affiche du film.

    ???+warning "Utilisation de [jsonschema2pojo](https://www.jsonschema2pojo.org/){target=_blank}"

        Lorsque vous utilisez cet outil pour générer le modèle utile pour utiliser le WS REST, il faut sélectionner **JSON-B 2.x** pour le style des annotations.

        En effet, les annotations sont ici **obligatoires**, car les noms des champs en JSON possèdent des `#`.

    ???+example "BONUS"

        Le clic sur le nom du film permet d'afficher une page ouvrant la bande annonce du film (si elle existe).

        Vous trouverez sur [cette page](https://imdb.iamidiotareyoutoo.com/docs/index.html){target=_blank} toutes les services que propose cette API, notamment comment obtenir le *trailer* d'un film.


## 3. Création d'une API REST

???+exercice "Exercice 3 - Création d'une API REST"

    - Afficher la liste des films.

        Il sera possible d'appliquer deux filtres (possibilité d'appliquer un seul des 2 ou les 2) :

        + Sur le titre : pour ce filtre, il s'agit d'un filre type *contains* insensible à la casse.
        + Sur la note : pour ce filtre, il s'agit de ne renvoyer que les films ayant la note indiquée.

        Il sera également possible de trier la liste sur la note, soit dans l'ordre croissant, soit dans l'ordre décroissant.

    - Afficher un film particulier avec son *id*.
    - Mise à jour partielle des informations (celles qui sont stockées en base uniquement) d'un film en particulier.
    - Mise à jour totale des informations (celles qui sont stockées en base uniquement) d'un film en particulier.
    - Création d'un film.
    - Suppression d'un film.

    ???+tip "Concernant l'accès en lecture aux données"

        Voici les champs qu'il faudra afficher :

        - Le titre
        - La note
        - Les acteurs principaux
        - L'année de sortie
        - Le lien vers l'image correspondant à l'affiche du film
    
    ???+tip "Concernant l'accès en écriture aux données (mise à jour, création et suppression)"
    
        L'utilisateur doit être "*authentifié*" (comme nous avons fait dans le TD 10, avec la valeur 42).

        Concernant la création ou la mise à jour, seules les informations stockées dans la base de données peuvent être indiquées.

    La mise en place de cette API devra bien sûr respecter les différentes conventions dont nous avons parlé dans ce cours.

    ⚠️⚠️ Cette API ne traitera et ne renverra des données que représentées en XML ou en JSON. Les autres formats ne seront pas accepté et une erreur correspondante sera envoyée.

    **Vous rendrez un document précisant les différentes URLs (*endpoint*) mises en place et comment accéder aux différents services.**

