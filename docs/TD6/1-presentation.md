---
author: Benoît Domart
title: Présentation de JPA
---

# Présentation de JPA

Nous allons ici reprendre l'application de gestion des notes développée dans le TD précédent, mais nous allons complétement changer la couche d'accès aux données. Nous n'allons plus faire cela "_à la main_", en écrivant les requêtes SQL et en faisant le _mapping_ entre le `ResultSet` et notre modèle (les _beans_).

Nous allons utiliser _JPA_ (pour _Java Persistence API_). Cela permet (entre autre) de s'abstraire de la base de données. Les requêtes écrites dans le TD5 sont en SQL. Si on se connecte à une base de données où le langage est différent (par exemple des fichiers XML), il faut tout refaire ...

Pour utiliser JPA, il va nous falloir un serveur d'application JEE. Tomcat n'en est pas un, c'est uniquement un serveur Web, contenant un **conteneur de _servlet_**. Nous allons pour cela utiliser **WildFly**.

??? note "Le serveur d'application *WilFly*"

    WildFly est le nouveau nom de JBoss depuis la version 8.<br><br>
    Il existe de nombreux serveurs d'application, qui offre les mêmes fonctionnalités, Weblogic d'Oracle, Websphere d'IBM, Glassfish, TomEE d'Apache ...

Le but de ce TD est d'implémenter la 2ème architecture vue en cours, à savoir :
<figure markdown>
  ![Architecture TD6](../images/schema_archi_V2.png)
  <figcaption>Schéma de l'architechture mise en place dans ce TD (JPA)</figcaption>
</figure>
