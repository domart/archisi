---
author: Benoît Domart
title: Exercice 1 - Installation de l'environnement de développement
---

# Exercice 1 - Installation de l'environnement de développement

## 1. Création et démarrage du serveur WildFly

Nous allons refaire ce que nous avions fait au tout premier TP en téléchargeant Tomcat 10, et en le configurant dans **vcode**, mais avec WildFly cette fois-ci.

Il faut commencer par télécharger le serveur WildFly, ici : [https://www.wildfly.org/downloads/](https://www.wildfly.org/downloads/){target=_blank}. Il suffit de cliquer sur **_DOWNLOAD THE ZIP_**, puis de le dézipper dans le dossier 📂`jee/serveurs`. Dans la suite, on appelera 📂`<WILDFLY_HOME>` le dossier où WildFly a été dézippé (c'est-à-dire 📂`jee/serveurs/wildfly-x.y.z.Final` normalement).

Il faut maintenant créer l'instance du serveur Wilfly dans vscode. Pour cela, dans la vue **SERVERS**, faire un clic droit et **Create New Server...**, mais cette fois-ci sur *Red Hat Server Connector* !

À la question **Download server?**, répondre **No, us server on disk** puisqu'ici aussi, on vient de le télécharger, puis indiquer 📂`<WILDFLY_HOME>` comme emplacement du serveur.

???+warning "Version de WildFly"

    Au moment où ceci est écrit, l'extension *Red Hat Server Connector* (sur VSCodium en tout cas) ne semble pas gérer les versions de WildFly supérieure à la 30.0.0.

    C'est donc celle-ci que je vous conseille de télécharger :

    <center>
        ![](./wildfly-version.png){width=60%}
    </center>

Tu peux maintenant démarrer le serveur, et vérifier qu'il est disponible ici : [http://localhost:8080/](http://localhost:8080/){target=_blank}. L'image de chat (Tomcat) n'est plus là, il y a maintenant un papillon :

<center>
    [![WildFly](../images/wildfly2.svg){width=30%}](http://localhost:8080/){target=_blank}
</center>

???+warning "Port déjà utilisé"

    Attention, si le serveur Tomcat n'a pas été arrêté, tu ne peux pas démarrer le server WildFly !

    En effet, par défaut, les deux tournent sur le port 8080. Tu obtiens alors l'erreur suivante :

    ``` title="📋 Console"
    Adresse déjà utilisée localhost/127.0.0.1:8080
    ```

    Il faut donc commencer par éteindre le serveur Tomcat !

## 2. Accès à la console d'administration du serveur

On peut maintenant démarrer le serveur depuis eclipse. Attention à bien vérifier que le serveur tomcat est arrêté. Les deux étant accessibles via le même port par défaut (8080), il y aura un problème si le tomcat est démarré.

Une fois le serveur démarré, on accède à sa page d'accueil via [http://localhost:8080](http://localhost:8080){target=_blank}. Lorsqu'on clique sur le lien ***Administration Console***, un nom d'utilisateur et un mot de passe sont demandés. Il n'y en a pas par défaut, il va falloir en créer un.

Pour cela, il faut exécuter le script 📄`<WILDFLY_HOME>/bin/add-user.bat` sous windows, ou 📄`<WILDFLY_HOME>/bin/add-user.sh` sous linux.

Nous allons créer un *Management User* (a), `admin/admin`. Pour des raisons de sécurité, cet utilisateur/mot de passe est **fortement** déconseillé, mais ici, il s'agit uniquement de développement, donc nous allons tout de même le faire. Ce script nous demandera de ce fait de confirmer notre choix. Il faut donc entrer, dans l'ordre, `a / admin / a / admin / yes / admin / <vide> / yes`, comme ci-dessous :
<center>
    ![Add user](../images/WildFly-adduser.png)
</center>

On peut maintenant accéder à la console d'administration (même sans redémarrer le serveur).
