---
author: Benoît Domart
title: Exercice 2 - Configuration de la connexion à la base de données
---

# Exercice 2 - Configuration de la connexion à la base de données

Nous allons maintenant configurer la _datasource_.

???+success "Schéma"

    Il faut bien sûr pour cela qu'une base de données soit accessible. On supposera ici qu'elle l'est sur `localhost` sur le port `3306`. **On suppose également qu'un schéma `gestion_notes` est créé sur cette instance.**
    
    Il a normalement été créé dans le TD précédent.

## 1. Configuration du pilote

Il faut commencer par installer le pilote MySQL. Par défaut, il n'y a que le pilote h2.

Pour cela, dans le dossier 📂`<WILDFLY_HOME>/modules/system/layers/base/com`, créer un dossier 📂`mysql` puis un dossier 📂`main` dans celui-ci. Il y a deux choses à mettre dans ce dossier :

1. **Le connecteur java-mysql (jar)**

    ???+success "Pilote pour MySQL"
        
        Nous l'avons déjà utilisé dans le TD précédent. Mais il suffisait de l'ajouter dans la configuration *Maven* (dans le fichier 📄`pom.xml`).
        
        Il est disponible [ici](https://dev.mysql.com/downloads/connector/j/){target=_blank} [(https://dev.mysql.com/downloads/connector/j/)](https://dev.mysql.com/downloads/connector/j/){target=_blank}.
        
        À ce lien, sélectionner "*Plateform Independent*", puis télécharger le zip.
        
        Il n'est pas nécessaire de se connecter, si tu n'as pas de compte Oracle, il suffit de cliquer sur *No thanks, just start my download* juste en dessous.
        
        ⚠️ Dans ce zip, seul le fichier 📄`mysql-connector-j-a.b.c.jar` nous intéresse. Tout les autres fichiers sont inutiles pour nous.
        
    Le connecteur 📄`mysql-connector-j-a.b.c.jar` doit être placé dans 📂`<WILDFLY_HOME>/modules/system/layers/base/com/mysql/main` (il suffit normalement de créer les dossiers 📂`mysql` et 📂`main`).

2. **Le fichier `module.xml`**

    ???+success "Le fichier module.xml"

        Créer ce fichier dans le dossier, en recopiant le contenu ci-dessous dans son contenu (⚠️⚠️**attention à adapter la version du jar**⚠️⚠️) :
        ``` xml title="📄 module.xml" linenums="1"
        <module xmlns="urn:jboss:module:1.5" name="com.mysql">
            <resources>
                <resource-root path="mysql-connector-j-8.2.0.jar" /><!--(1)!-->
            </resources>
            <dependencies>
                <module name="javax.api"/>
                <module name="javax.transaction.api"/>
            </dependencies>
        </module>
        ```

        1. ⚠️⚠️
        
            Il faut indiquer ici le même nom que celui du jar présent dans ce dossier.
            
            ⚠️⚠️

3. **Dans la console d'administration**.

    ???+success "Dans la console d'administration"

        Dans la [console d'administration](http://localhost:9990/console/index.html){target=_blank}, aller dans le menu `Configuration > Subsystems > Datasources & Drivers > JDBC Drivers`.

        Il ne doit pour l'instant n'y en avoir qu'un seul : `h2`.

        Cliquer sur "Add JDBC Driver", puis remplir :

        - Driver Name : `mysql`
        - Driver Module Name : `com.mysql`

        Le reste est laissé vide. Cliquer enfin sur <bouton>Add</bouton>
        
        <center>
            ![Installation Wildlfy - Pilote JDBC](../images/install_wildfly_6.png)
        </center>

    ???note "Si besoin, modification manuelle du fichier 📄standalone.xml"

        Plutôt que d'utiliser la console d'administration, il est possible de faire ces mêmes modifications directement depuis un fichier de configuration XML. Il s'agit du fichier 📄`WILDFLY_HOME/standalone/configuration/standalone.xml`.

        Dans ce fichier, chercher la balise `<drivers>`. Elle contient uniquement le driver `h2` pour le moment. Nous allons ajouter notre driver `MySQL`. Pour cela, recopier le code suivant :
        ``` xml title="📄 standalone.xml" linenums="1"
        <driver name="mysql" module="com.mysql"/>
        ```

## 2. Configuration de la source de données

???+success "Configuration de la source de données"

    Nous pouvons maintenant crééer la _datasource_.

    Toujours depuis la [console d'administration](http://localhost:9990/console/index.html){target=_blank}, **après avoir redémarré le serveur Wildfly**, aller dans :fontawesome-solid-bars:`Configuration > Subsystems > Datasources & Drivers > Datasources`, et cliquer sur "Add Datasource". Il y a 6 écran de configuration :

    1. Sélectionner "MySQL".
    2. Indiquer `MySqlGestionNotesJPA` comme Name et `java:/MySqlGestionNotesJPA` comme JNDI Name.
    3. Ne rien changer.
    4. Indiquer `jdbc:mysql://localhost:3306/gestion_notes` dans _Connection URL_ (il faut adapter cette URL si le port est différent, et si le schéma créé sur la BDD n'est pas `gestion_notes`), et saisir le _User Name_ et l'éventuel _Password_.
    5. Tester la connexion.
    6. Valider la création de la _datasource_.

???note "Si besoin, modification manuelle du fichier 📄standalone.xml"

    Nous avons fait la même opération que lorsque nous avions modifié les fichiers 📄`server.xml` et 📄`context.xml` sur le serveur Tomcat.

    Ici, on aurait également pu modifier directement le fichier 📄`standalone.xml`, plutôt que d'utiliser la console d'administration.

    Dans ce fichier de configuration, vous pouvez constater l'apparition d'une balise `<datasource>` référencant la source `MySqlGestionNotesJPA`.