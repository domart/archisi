---
author: Benoît Domart
title: Exercice 3 - Les cookies
tags:
  - 0-simple
---

# Exercice 3 - Les cookies

???+success "Rappel"

    - Un **cookie** est une information envoyée par le serveur Web au client (le navigateur), qui peut ensuite être relue par le client.

    - Lorsqu'un client reçoit un cookie, il le sauve (sous la forme d'un fichier) et le renvoie ensuite au serveur **chaque fois** qu'il accède à une page sur ce serveur (plus précisément, à chaque fois qu'il effectue une requête HTTP sur ce serveur).

???+info "L'objet Cookie"

    L'API Servlet fourni un objet **Cookie** (`jakarta.servlet.http.Cookie`), dont voici quelques méthodes utiles (la Javadoc est [ici](https://docs.oracle.com/javaee/7/api/javax/servlet/http/Cookie.html){target=_blank}) :

    - `Cookie(String name, String value)` : construit un cookie.
    - `String getName()` : retourne le nom du cookie.
    - `String getValue()` : retourne la valeur du cookie.
    - `setValue(String newValue)` : donne une nouvelle valeur au cookie.
    - `setMaxAge(int expiry)` : spécifie l’âge maximum du cookie.

    On ajoute un cookie donné à la réponse HTTP via `HttpServletResponse.addCookie(Cookie monCookie)`.

    On récupère les cookies présents dans la requête HTTP via `Cookie[] HttpServletRequest.getCookies()`. Cette méthode renvoie un tableau de `Cookie`, qui peut être `null` si aucun cookie n'est présent dans la requête.

    Exemple :
    ```java
    Cookie[] cookies = request.getCookies();
    if (null != cookies) {
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName());
            System.out.println(cookie.getValue());
        }
    }
    ```

???+exercice "Question 1 - Mise en place d'un cookie de connexion"

    Crée une Servlet `CookieServlet`, implémentant la méthode `doGet` pour gérer les cookies :

    - Si le client est déjà connu, alors le message :

        `Encore vous !`

        est affiché.

    - Sinon un cookie dont le nom est `id_mon_site` est ajouté à la réponse HTTP. Le message :
        
        `Bienvenue !`

        est alors affiché.
    
    ???+tip "Générer un identifiant unique"

        Pour générer un identifiant unique, on utilisera l'objet `UID` de `java.rmi.server` :

        ```java
        UID idMonSite = new UID();
        String id = idMonSite.toString();
        ```

    ???tip "Aide"

        Pour savoir si le client est déjà connu, on regardera si un cookie dont le nom est `id_mon_site` existe dans la requête.

    <center>
        ![Exemples](./exemple_cookie.gif)
    </center>

    ???+success "Pour tester ..."

        Pour pouvoir tester ce que tu as fait ici, il faut :
        
        - soit supprimer les cookies à chaque nouveau test,
        - soit te mettre en "_navigation privée_", les cookies sont ainsi automatiquement supprimés lorsque la fenêtre est fermée ...


???+exercice "Question 2 - Pour aller plus loin"

    Modifier le code pour que le cookie soit désactivé au bout de 5 secondes, c'est-à-dire que si on met plus de 5 secondes avant de raffraîchir la page, le message affiché est à nouveau "Bienvenue !".

    <center>
        ![Exemples](./exemple_cookie_2.gif)
    </center>