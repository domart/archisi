---
author: Benoît Domart
title: Exercice 2 - Une Servlet avec des paramètres
tags:
  - 0-simple
---

# Exercice 2 - Une deuxième _Servlet_, avec des paramètres

On reprend ici la _Servlet_ de l'exercice précédent. Nous allons modifier la méthode `doGet` pour qu'elle redirige vers une page HTML contenant un formulaire. Ce formulaire, à sa soumission, déclenchera une requête HTTP POST, avec un paramètre.

Voici ce qu'il faut mettre en place :

<center>
    ![](./exercice2.gif){width=70%}
</center>


1. Commencons donc par créer la page 📄`premierFormulaire.html` (dans le dossier 📂`webapp`) :
    ```html linenums="1"
    <html>
        <head>
            <title>1er formulaire</title>
        </head>
        <body>
            <form method="post" action="maPremiere"><!--(1)!-->
                <table>
                    <tr>
                        <td>Prénom</td>
                        <td><input type="text" name="prenom"/></td><!--(2)!-->
                    </tr>
                    <tr>
                        <td>Mot de passe</td>
                        <td><input type="password" name="mdp"/></td><!--(3)!-->
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Valider"/></td><!--(4)!-->
                    </tr>
                </table>
            </form>
        </body>
    </html>
    ```

    1. Le formulaire HTML (balise `<form>`) permet au client d'envoyer des informations au serveur via une requête HTTP.<br>
        Ici, ce sera une requête POST (attribut `method`), qui exécutera l'URL [/maPremiere](/maPremiere) (attribut `action`).
    2. Les balises `<input type="xxx">` permettent  à l'utilisateur de saisir des informations dans le formulaire. Elles seront placées en paramètres de la requête, avec comme clef la valeur de l'attribut `name`.
    3. Les balises `<input type="xxx">` permettent  à l'utilisateur de saisir des informations dans le formulaire. Elles seront placées en paramètres de la requête, avec comme clef la valeur de l'attribut `name`.
    4. La balise `<input type="submit">` ajoute un bouton qui va permettre de soumettre le formulaire, c'est-à-dire d'exécuter la requête correspondant à ce qui a été indiqué dans la balise `<form>`.

2. Modifier la méthode `doGet` pour qu'elle ne fasse que la redirection vers la page 📄`premierFormulaire.html` :
    ``` java title="☕ Code Java - MaPremiereServlet.doGet" linenums="1"
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("premierFormulaire.html"); //(1)!
        dispatcher.forward(request, response); //(2)!
    }
    ```

    1. Indique vers quelle ressource la requête devra être redirigée, ici vers 📄`premierFormulaire.html`.
    2. Effectue la redirection (toutes les informations de la requête et de la réponse sont redirigées).

    ???+note "Accès au formulaire"

        Cela signifie que nous accéderons au formulaire via l'URL [http://localhost:8080/td2/maPremiere](http://localhost:8080/td2/maPremiere){target=_blank}, qui permettra l'exécution de cette Servlet, qui nous redirigera vers le formulaire.

        On pourrait également directement y accéder via [http://localhost:8080/td2/premierFormulaire.html](http://localhost:8080/td2/premierFormulaire.html){target=_blank}

        L'avantage de cette méthode est qu'elle permet d'effectuer des actions avant l'affichage du formulaire, comme par exemple le pré-remplissage de certains champs en fonction de ce que l'utilisateur a pu faire précédemment.

        Pour ceux qui sont déjà familier avec le paradigme MVC, cela revient à dire qu'on sépare la partie contrôleur (la Servlet) et la partie vue (la page HTML).

3. Dans la _Servlet_ `MaPremiereServlet`, ajouter la méthode `doPost` :
    ``` java title="☕ Code Java - MaPremiereServlet.doPost" linenums="1"
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        // Récupération de la valeur du champ dont l'attribut "name" est "prenom".
        String prenom = request.getParameter("prenom"); //(1)!
        // Génération du code HTML.
        PrintWriter out = response.getWriter();
        out.println("<html><head></head><body>Salut " + prenom + " !</body></html>");
    }
    ```

    1. Il faut qu'une des balise `<input type="xxx">` du formulaire est _prenom_ comme valeur pour l'attribut `name`.

4. Vérifier le bon fonctionnement à cette [URL](http://localhost:8080/td2/maPremiere){target=_blank}.

    ???+success "Problème d'encodage"

        Par défaut, tu as certainement un problème avec l'accent de "prénom".

        Pour le corriger, il faut indiquer au navigateur quel ensemble de caractères utiliser. Le plus complet est [UTF-8](https://fr.wikipedia.org/wiki/UTF-8){target=_blank}.

        Pour indiquer qu'on utilise cet encodage, il faut ajouter la balise `<meta>` ci-dessous, à l'intérieur de la balise `<head>` :

        ```html title="À insérer dans la balise head de la page HTML"
        <meta charset="UTF-8"/>
        ```

???+ warning "Attention, on ne fera en fait pas ça en pratique !"

    En pratique, on n'écrira en fait **jamais** de code HTML dans une _Servlet_ via l'objet `PrintWriter`. On redirigera **toujours** la requête vers une page _JSP_.

    C'est un peu l'idée du paradigme MVC, on ne mélange pas le contrôleur (la _Servlet_) et la vue (les pages HTML et les _JSP_ dont nous allons parler juste après).

    Ce que nous avons fait ici a **uniquement un but pédagogique**, pour comprendre les différents enchaînements.

En conclusion, voici un schéma de ce que nous avons mis en place dans cet exercice, et que nous reproduirons systématiquement par la suite :

<figure markdown>
  ![Architecture TD2 - Exercice 2](../images/schema_MVC_Intro.png)
  <figcaption>Schéma de la cinématique mise en place - La requête HTTP peut être GET (clic sur un lien) ou POST (clic sur le bouton "Valider" d'un formulaire)</figcaption>
</figure>