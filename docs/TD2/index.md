# TD2 - Les Servlets 🖧

L'objectif de ce TD est de créer un projet Web contenant des _Servlets_, de les manipuler, et de bien comprendre leur fonctionnement.

Concrétement, nous allons mettre en place une architecture de ce type :
<center>![archi td2](../images/archi-2tiers-4.png)</center>