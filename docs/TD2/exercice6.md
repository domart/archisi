---
author: Benoît Domart
title: Exercice 6 - Verbe dynamique
tags:
  - 0-simple
---

# Exercice 6 - Verbe dynamique

Crée une page HTML contenant une liste déroulante avec différents verbes HTTP (GET, POST, PUT et DELETE), et un bouton <bouton>Valider</bouton>.

???+info "Remarque"

    Ici, vous pouvez **EXCEPTIONNELLEMENT** accéder directement à cette page HTML, sans passer par une Servlet pour l'afficher.

    On utilisera donc une URL du type [http://localhost:8080/td2/choixVerbe.html](http://localhost:8080/td2/choixVerbe.html){target=_blank} pour y accéder.

    De même, **EXCEPTIONNELLEMENT**, on écrira ici du code HTML directement dans la Servlet.

    Il s'agit ici de gagner un peu de temps pour aller à l'essentiel.

Le clic sur ce bouton doit rediriger vers la page correspondante :

<center>
    ![Exemples](./exemple_choix_verbe.gif)
</center>

La Servlet appelée lors du clic sur le bouton <bouton>Valider</bouton> doit contenir les méthodes suivantes, **qui ne doivent pas être modifiées** :

```java
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.setContentType("text/html");
    response.getWriter().append("<html><head></head><body>Verbe GET appelé</body></html>");
}

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.setContentType("text/html");
    response.getWriter().append("<html><head></head><body>Verbe POST appelé</body></html>");
}

@Override
protected void doPut(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.setContentType("text/html");
    response.getWriter().append("<html><head></head><body>Verbe PUT appelé</body></html>");
}

@Override
protected void doDelete(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.setContentType("text/html");
    response.getWriter().append("<html><head></head><body>Verbe DELETE appelé</body></html>");
}
```