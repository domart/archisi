---
author: Benoît Domart
title: Exercice 4 - Connexion
tags:
    - 0-simple
---

# Exercice 4 - Page de connexion

???+success "Servlet et code HTML"

    À partir de maintenant on utilisera toujours une Servlet pour afficher du contenu Web, c'est-à-dire une page HTML (pour le moment - nous parlerons bientôt des pages JSP).

    De même, on n'écrira jamais du code HTML dans une Servlet. La Servlet effectue des traitements (éventuellement) et redirige ensuite vers le contenu web sélectionné.

En repartant du code de l'exercice 2, créer une servlet `ConnectionServlet` permettant à l'utilisateur d'être redirigé vers :

- une page HTML affichant "Salut !", si le mot de passe saisi est `password`,
- une page HTML affichant à nouveau le formulaire, ainsi qu'un message d'erreur en rouge indiquant :

    <span style="color:red">Nom d'utilisateur ou mot de passe erroné</span>
    
    si le mot de passe saisi n'est pas `password`.

<center>
    ![Exemples](./exemple_connexion.gif)
</center>