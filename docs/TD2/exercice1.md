---
author: Benoît Domart
title: Exercice 1 - Une première Servlet
tags:
  - 0-simple
---

# Exercice 1 - Une première _Servlet_

Maintenant que nous savons créer un projet Web, nous allons pouvoir créer des _Servlets_.

1. Créer un nouveau projet, appelé `td2`, toujours avec l'*archetype* Maven `webapp-jakartaee10`, et toujours dans le *groupid* `fr.univtours.polytech`.
1. N'oubliez pas de de rajouter la balise `<finalName>` dans le 📄`pom.xml`, avec la valeur `td2`.
1. Dans ce projet `td2`, aller dans le dossier 📂`src/main/java`, qui va contenir toutes les sources.
2. Créer le package `fr.univtours.polytech.td2.servlet` (normalement, le package `fr.univtours.polytech.td2` a déjà été créé par *Maven* lors de la création du projet, il suffit donc de créer le sous-package `servlet`).
3. Dans ce package, créer la classe : `MaPremiereServlet`.

    ???+ success "Remarque"

        Il est pratique d'indiquer ce qu'est la classe dans son nom, ici, une _Servlet_.
    
    ???+hint "Créer une classe Java"

        Si la vue **Java Projects** n'est pas encore activée, il faut faire un clic droit sur le nom du dossier dans lequel créer la classe, puis `Nouveau fichier Java > Classe...`.

4. Indiquer que la classe hérite de `HttpServlet` de l'API `servlet` (en ajoutant `extends HttpServlet` après le nom de classe).

    ???+ warning "Attention !"

        Attention, si vscode ne trouve pas la classe (abstraite) `HttpServlet`, c'est certainement que tu n'as pas bien configuré le projet, peut-être en n'indiquant pas le bon archetype maven.

        Il faut alors reprendre la création du projet maven vue dans le TD1.

        ⚠️ Dans tous les cas, ici, il ne faut pas ajouter "manuellement" les JARs. La gestion des dépendances est effectuée par *Maven*, dans le fichier 📄`pom.xml`.
    
    ???+success "Classes et interfaces importées automatiquement"

        Tous les imports sont automatiques. Il faut pour cela utiliser l'autocomplétion, qu'on obtient avec ++ctrl+space++.

        Ici, il suffit donc dans la déclaration de la classe d'ajouter après son nom `extends Https`, puis d'appuyer sur entrée après avoir sélectionné `HttpServlet`.

        Si tu ne fais pas ainsi, l'import n'est pas automatiquement ajouté ! 

5. Recopier le contenu suivant :

    ``` java title="☕ Code Java - Servlet MaPremiereServlet" linenums="1"
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter(); //(1)!
        out.println("<html><head></head><body>Hello World !</body></html>"); //(2)!
    }
    ```

    1. Cet objet va nous permettre d'écrire dans la réponse HTTP.
    2. On écrit directement le code HTML dans la réponse, qui va s'afficher dans le navigateur.

    ???+success "Surcharger des méthodes"

        Nous venons ici d'implementer la méthode `void doGet(HttpServletRequest, HttpServletResponse)`.

        **vscode** permet de faire cela directement via 🖱️`Source Action...`, puis `Override/Implements Methods`, puis en sélectionnant les méthodes que l'on souhaite implémenter et/ou surcharger.
    
    ???+note "Donner des noms de variables explicites !"

        Par défaut, lorsqu'on surcharge la méthode `doGet` par exemple, les noms des paramètres sont `req` et `resp`.

        **Prend systématiquement le temps de le renommer** en `request` et `response` !

        Dès que le code devient compliqué, il est très important d'avoir des noms de variables **clairs** et **explicites** ! Avec l'autocomplétion, cela ne prend en plus pas plus de temps pour les écrire ...
    
    ???+hint "Ajouter des raccourcis à vscode"

        Par défaut, il n'y a aucun raccourci affecté à l'affichage de `Source Action...`. Mais il est possible d'en rajouter un !

        Pour cela, il faut ouvrir le fichier `keyBindings.json`, qui recense tout vos raccourcis personnalisés.

        - Si vous ne l'avez jamais ouvert, il faut faire ++ctrl+"k"++ ; ++ctrl+"s"++, puis cliquer sur l'icône tout en haut à droite pour afficher le fichier JSON.
        - La prochaine fois, il suffira de faire ++ctrl+"p"++, puis saisir `keybindings.json`.

        Dans ce fichier, ajouter le code suivant pour que le raccourci ++ctrl+shift+"a"++ permette cette action :

        ```json
        // Placer vos combinaisons de touches dans ce fichier pour remplacer les valeurs par défaut
        [
            // Raccourcis précédents ...
            {
                "key": "ctrl+shift+a",
                "command": "editor.action.sourceAction"
            },
            // Raccourcis suivants ...
        ]
        ```

    ???+note "Remarque"

        En programmation, la tradition veut que le premier programme rédigé dans un langage donné soit l'affichage d'[_Hello World !_](https://fr.wikipedia.org/wiki/Hello_world){target=_blank}.

        C'est ce que nous allons faire ici, en affichant ce message dans notre navigateur.
    
    ???+success "Bonne pratique"

        Lorsqu'on utilise **vscode**, on prendre l'habitude, **à chaque modification** de code Java, d'effectuer deux actions :

        1. ++shift+alt+"O"++ : Permet de faire du ménage dans les imports en important ceux nécessaires, et en supprimant ceux qui ne sont pas utilisés.
            
            Les `import java.io.*` par exemple sont remplacés par l'import des classes utilisées uniquement.
            
            Sans cette action, des erreurs sont signalées ici.
        2. ++ctrl+shift+"P"++ puis saisir **format document** : Permet de formater le code, de la même façon pour tout le monde.<br>C'est vraiment un plus si le code de tout le monde est formaté de la même façon. Cela permet de le comprendre beaucoup plus rapidement !


8. C'est bon, notre _Servlet_ est **créée** ! Il n'y a plus qu'à la **déployer**.

    Pour cela, il suffit d'ajouter l'annotation suivante sur notre classe :

    ```java title="Annotation pour déployer la Servlet"
    @WebServlet(name="maPremiereServlet", urlPatterns = {"/maPremiere"})
    ```

    - Pour le paramètre `name`, on peut indiquer ce qu'on veut. Il s'agit du nom que l'on donne à la servlet déployée. Nous ne l'utiliserons pas ici. On donnera le nom de la classe, avec la première lettre en minuscule.
    - Pour le paramètre `urlPatterns`, il s'agit d'une liste (d'où les `{}`) des URLs qui seront redirigées vers cette *Servlet*.

        Avec cette configuration, cela signifie que l'URL [http://localhost:8080/td2/maPremiere](http://localhost:8080/td2/maPremiere){target=_blank} permet d'exécuter cette *Servlet*.

9. Après avoir **réexécuter la compilation Maven**, vérifier que "Hello World" est bien affiché à cette [http://localhost:8080/td2/maPremiere](http://localhost:8080/td2/maPremiere){target=_blank} (il faut bien sûr que le serveur Tomcat soit démarré et que le projet Java soit déployé dessus).

    ???+ success "Prise en compte des modifications"

        Pour que les modifications effectuées sont bien prises en compte, il faut, **après chaque modification** ré-exécuter `mvn clean install`.