---
author: Benoît Domart
title: Exercice 2 - Création d'un projet Web dynamique
tags:
  - 0-simple
---

# Exercice 2 - Création d'un projet Web dynamique

Maintenant que le serveur est démarré, nous allons pouvoir créer un projet Web et le déployer dessus.

Maven offre la possibilité de créer plusieurs types de projets Java. Nous allons donc créer ici un projet Web, dans lequel nous pourrons par la suite (dans le TD2) créer et déployer des _Servlets_.

???+success "Création du projet Web"

    1. On commence donc par créer un projet Web :

        Pour cela, il faut aller fans la vue **MAVEN** et cliquer sur le bouton "**+**". Dans la zone de saisie, après avoir cliqué sur `more` (attention ! il faut cliquer sur `more`, sinon vous ne pourrez pas sélectionner toutes les options nécessaires !), sélectionner `webapp-jakartaee10`.

        C'est toujours cet *archetype* que nous utiliserons dans ce cours.

        Si la vue **MAVEN** n'apparaît, pas, lance l'exécuteur de commande, via ++ctrl+shift+"P"++, puis saisir `Maven: New Project...`,

    1. Après avoir validé, sélectionner la dernière version (1.1.0 au moment où ceci est rédigé), puis indiquer le **group id** de notre projet, c'est-à-dire l'identifiant de la structure à laquelle le projet appartient. Ici, nous allons indiquer `fr.univtours.polytech`. Il s'agit du domaine internet de la structure (sans tiret), écrit à l'envers.

    1. Il faut ensuite indiquer le nom du projet. Ici, nous allons saisir `td1`.

        Tous les packages de notre projet commencerons donc par `fr.univtours.polytech.td1`.

    1. Il faut maintenant indiquer où le projet, c'est-à-dire ici le dossier 📂`td1`, sera stocké sur le disque dur.

        Il faut donc indiquer 📂`JEE/ws_archisi`.

    1. Enfin, pour terminer, dans le terminal de **vscode**, il faut valider certains choix : le numéro de version de notre projet et le début des packages (il suffit de ne rien saisir et de valider à chaque fois pour conserver les valeurs par défaut, qui nous conviennent).

???+hint "Maven"

    Maven permet d'automatiser de nombreuses tâches dans la gestion d'un projet Java.

    Il permet par exemple de gérer toutes les dépendances du projet, c'est-à-dire toutes les bibliothèques qu'il faut charger dans le *classpath*.

    Il permet également d'automatiser la génération du livrable, c'est-à-dire dans notre cas (et pour l'instant), du fichier WAR que nous allons ensuite déployer sur le serveur. C'est ce que nous allons voir juste ci-dessous.

    Toute la gestion de *maven* est effectuée dans le fichier 📄`pom.xml`, présent à la racine du fichier.

    Afin d'indiquer le nom du livrable que nous allons déployer sur le serveur Tomcat, ouvrir ce fichier 📄`pom.xml` et ajouter à l'intérieur de la balise `<build>` la balise suivante :

    ```xml title="Dans le 📄pom.xml"
    <finalName>td1</finalName>
    ```

    Ainsi, le livrable généré s'appellera `td1.war`. Sans cette modification, le livrable s'appelle `td1-1.0-SNAPSHOT.war`.

    !!!success "Attention !"

        L'ajout de cette balise dans le fichier 📄`pom.xml` sera à faire à chaque création d'un nouveau projet !

???+ note "Racine de l'application Web"

    Par défaut, le _context root_ de l'application est le nom du projet, ici **_td1_** (si tu as fais la configuration indiquée juste ci-dessus).<br>
    Cela signifie que pour accéder au contenu de l'application depuis un navigateur Web, **une fois que celle-ci sera déployée sur un serveur**, toutes les URLs seront du type [http://localhost:8080/td1/...](http://localhost:8080/td1/...){target=_blank}

    Cela signifie également qu'on peut déployer autant de projet Web que souhaité sur le serveur tomcat, puisqu'ils auront tous un _context root_ différent, et ils auront donc tous une URL différentes pour y accéder.

???+ note "Arborescence d'un projet Web dynamique"

    Maven permet de créer automatiquement un projet Java, avec l'arborescence suivante :

    <pre style="line-height: 1.2;">
    td1/
    └── src/
        ├── main/
        │   ├── java/
        │   │   └── ici, il y a les packages (qui sont des dossiers) et les classes Java.
        │   ├── ressources/
        │   │   └── ici, il y a les fichiers de configuration de l'application Web.
        │   └── webapp/ (c'est le contenu Web de l'application)
        │       ├── WEB-INF/
        │       │   └── web.xml
        │       └── ici, il y a toutes les pages HTML, JSP, CSS, JavaScript ...
        │           éventuellement dans des sous-dossiers.
        └── pom.xml       (c'est le fichier de configuration Maven)
    </pre>

    Ainsi, l'URL [http://localhost:8080/td1/](http://localhost:8080/td1/){target=_blank} pointe vers le dossier 📂`webapp`.
    
    Par défaut, l'*archetype* Maven que nous avons utilisé à créé une page 📄`index.html` dans le dossier 📂`webapp`, on pourra afficher son contenu dans un navigateur en allant à l'URL [http://localhost:8080/td1/index.html](http://localhost:8080/td1/index.html){target=_blank}

<div id="creation-livrable"/>
???+success "Création du livrable avec Maven"

    Notre projet est un projet Web. Le livrable associé, c'est-à-dire le fichier que nous allons déployer sur le serveur, est un fichier **WAR**, pour _Web Application Archive_.

    Pour créer ce fichier, il suffit de se placer dans le dossier de notre projet, 📂`JEE/ws_archisi/td1`, et d'exécuter la commande `mvn clean install`.

    Cela créé un dossier 📂`target`, au même niveau que le dossier 📂`src`, qui contient entre autre le fichier 📄`td1.war`.

    ???+note "Avec l'interface graphique"

        Il est également possible d'aller dans la vue **MAVEN**, puis après avoir fait un clic droit sur le projet **td1**, de sélectionner **Run Maven Commands... > install**.

<div id="deploiement-application"/>
???+success "Déploiement du projet Web sur le serveur Tomcat"

    Sur ce fichier 📄`td1.war`, il suffit de faire un clic droit,

    1. Puis **Run on Server**,
    1. Puis sélectionner **Community Server Connector**,
    1. Puis **Tomcat 10.x**,
    1. Et enfin ne pas afficher le fichier de propriété.

    Le contenu du dossier 📂`webapp` est maintenant disponible ici : [http://localhost:8080/td1/](http://localhost:8080/td1/){target=blank}.

    ???+note "Remarques"

        - **À chaque modification du code**, il faudra ré-exécuter la commande `mvn clean install` afin de mettre à jour le 📄`td1.war`.

            Avec le serveur Tomcat, il faudra également le redémarrer pour que les modifications soient bien prises en compte.
        - Il est possible de déployer autant de projet Web que souhaité sur ce serveur Tomcat. Les URLs pour y accéder seront toutes différentes.

    Par défaut, cette URL pointe sur le fichier 📄`index.html`, et affiche donc "**Hello World**". Nous verrons plus tard comment modifier le contenu affiché par défaut.