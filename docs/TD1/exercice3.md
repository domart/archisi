---
author: Benoît Domart
title: Exercice 3 - Requêtes HTTP
tags:
  - 0-simple
---

# Exercice 3 - Requêtes HTTP

Maintenant que nous avons créé ce projet Web, nous allons le **compléter**, le **redéployer** et le **tester**.

## 1. Récupération d'un contenu Web

Télécharge le ZIP ci-dessous, et dépose son contenu dans le dossier 📂`webapp` du projet.

<center>
    [ContenuWeb.zip](./ContenuWeb.zip){ .md-button download="ContenuWeb.zip" }
</center>

???+success "Arborescence du projet"

    L'arborescence du projet doit donc maintenant être (avec **en gras** ce qui vient d'être ajouté) :

    <pre style="line-height: 1.2;">
    td1/
    └── src/
        ├── main/
        │   └── webapp/
        │       ├── **css/**
        │       │   └── **style.css**
        │       ├── **javascript/**
        │       │   └── **jscript.js**
        │       ├── WEB-INF/
        │       │   └── web.xml
        │       ├── index.jsp
        │       └── **testHttp.html**
        └── pom.xml
    </pre>

## 2. Études des requêtes HTTP

**Après avoir ré-exécuté la commande** `mvn clean install` (ce qu'il faudra faire après chaque modification de code), notre application contient une nouvelle page, **testHttp.html**, qui est disponible à l'adresse [http://localhost:8080/td1/testHttp.html](http://localhost:8080/td1/testHttp.html){target=_blank}

Accède à cette page HTML depuis Firefox (ou un autre navigateur), ouvre le menu `Réseau` (++ctrl+shift+"E"++ dans Firefox), puis rafraîchis la page (++ctrl+shift+"R"++).

1. Il doit y avoir 5 lignes (sinon rafraîchir à nouveau). Chacune correspond à une requête HTTP.

    Indiquer l'origine de chacune de ces requêtes par rapport au fichier HTML.

2. La première colonne donne le code de retour. Il y a une erreur. La corriger.

    ???+success "Les codes retours"

        Pour rappel, voici les 5 familles de codes retours :

        * 1XX : Réponses informatives.
        * 2XX : Succès.
        * 3XX : Redirection.
        * 4XX : Erreur côté client.
        * 5XX : Erreur côté serveur.

3. La page HTML contient un formulaire (balises `<form>...</form>`) dont la méthode est `GET`.

    ???+success "Un formulaire Web"

        Regarde au passage le code HTML de cette page. Elle contient un **formulaire** (définie par les balises `<form>` et `</form>`), qui va permettre à l'utilisateur de saisir des informations, et de les envoyer à un serveur.

        Différents types d'informations peuvent être renseignées dans un formulaire (du texte, des mots de passe, des fichiers, des checkbox, des radio boutons, ...), et à chaque fois, le code ressemble à `<input type="xxx" name="yyy"/>`.

        Côté serveur, la donnée saisie sera récupérée grâce à la valeur de la propriété `name` (qui doit donc être **différente** pour chaque champ).

    Vérifier que lorsqu'on soumet le formulaire (c'est-à-dire lorsqu'on clique sur <bouton>Valider</bouton>), le mot de passe entré apparaît "en clair" dans l'URL.
    
    1. Que faut-il modifier pour que ça ne soit plus le cas ?
    2. Combien l'URL contient-elle de paramètres ?
    3. Pour analyser une requête HTTP, il faut cliquer sur la ligne qui correspond dans la console de Firefox. On obtient alors plusieurs onglets, qui permettent notamment d'obtenir les en-têtes (de la question et de la réponse), les cookies, la requête, et la réponse.

        Avec la modification apportée précédemment, où sont maintenant indiqués les valeurs des différents paramètres (`leNomDUtilisateur`, `leMotDePasse`, ...) ?
        
    4. Quels sont les différents types de données que l'on peut mettre dans une formulaire HTML ?

        Ajoute les dans la page, et observe comment ces paramètres sont envoyés dans la requête HTTP.