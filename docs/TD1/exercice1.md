---
author: Benoît Domart
title: Exercice 1 - L'environnement de développement
tags:
  - 0-simple
---

# Exercice 1 - L'environnement de développement

## 1. Installation de l'environnement de développement

Première chose à faire avant de se lancer dans le développement : installer et configurer l'environnement de développement. Pour cela, crée quelque part sur ton poste un dossier 📂`JEE` dans lequel nous allons placer tous les outils nécessaires au développement de notre projet. Pour cette première version de notre site, nous allons avoir besoin de trois choses.

???+success

    Pour éviter d'éventuels problèmes, il est préférable de placer ce dossier 📂`JEE` dans des dossiers dont les noms ne contiennent pas d'espace, et dont le chemin complet n'est pas trop long. Bref, le plus simple est de le mettre à la racine de votre disque, dans 📂`C:` ou 📂`D:` par exemple.

    À la fin, ton dossier 📂`JEE` devra ressembler à ça :
    <pre style="line-height: 1.2;">
        JEE/
        ├── JDK/
        │   └── jdk11/
        ├── serveurs/
        │   └── apache-tomcat-10.x.y/
        └── ws_archisi/
    </pre>

1. La dernière version de **vscode** (ou de **vscodium**).

    On la trouve ici : [https://code.visualstudio.com/download](https://code.visualstudio.com/download){target=_blank}.

    On peut préférer utiliser **vscodium** qui est une version libre de **vscode** : [https://vscodium.com/](https://vscodium.com/){target=_blank}.

1. La version 11 de la **jdk**.


    ???+success "Télécharger la version 11"

        On la trouve ici : [https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html){target=_blank} (il faut un compte pour se connecter pour pouvoir télécharger).

        ???+warning "Attention"

            Avec ce lien, attention à bien sélectionner ton système d'exploitation (c'est Linux par défaut) !
        
        ???+warning "Attention !"

            Il faut bien utiliser la version 11 de la JDK. Des tests avec des versions plus récentes, ou plus anciennes, n'ont pas été très concluants ...

    === "Pour Windows"
    
        ???+success "Bien configurer Java sous Windows"

            Pour bien configurer Java, et savoir quelle est la version que tu utilises, voici ce qu'il faut faire :

            * Crée (ou modifie) la variable d'environnement `JAVA_HOME`, dont la valeur est le dossier où est la _JDK_.

                Pour être sûr que c'est le bon, c'est le dossier qui contient le sous-dossier 📂`bin` qui contient lui-même les exécutables ⚙️`java`, ⚙️`javaw`, ⚙️`javac` ...

                Par exemple, sous Windows, on aura quelque chose comme :

                <center>![Variable d'environnement JAVA_HOME](../images/var_env_java_home.png)</center>

            * Crée (ou modifie) la variable d'environnement `JRE_HOME`, dont la valeur est celle de `JAVA_HOME`.

                Sous Windows, il faut donc indiquer `%JAVA_HOME%` comme valeur.

                Par exemple, sous Windows, on aura quelque chose comme :

                <center>![Variable d'environnement JAVA_HOME](../images/var_env_jre_home.png)</center>
            
            * Modifie la variable `PATH` (⚠️ elle existe déjà !), en ajoutant **en premier** un lien vers le dossier 📂`bin` de la JDK (celui qui contient les exécutables).

                Par exemple, on aura quelque chose comme :

                <center>![Variable d'environnement JAVA_HOME](../images/var_env_path.png)</center>
                
                ???+warning "Attention !"
                
                    Attention à ne pas oublier le `\bin` dans `%JAVA_HOME%\bin` !
                
                ???+danger "Attention !!"

                    Attention à ne pas écraser ou supprimer cette variable d'environnement, cela pourrait entraîner des dysfonctionnements dans d'autres logiciels !
            
            * Enfin, ouvre un terminal et exécute la commande `java -version`

                Si tu obtiens quelque chose qui ressemble à ça :

                <center>
                ![Java Version](./java-version-11-linux.gif)
                </center>

                c'est bon signe !
                

    === "Pour Linux"
    
        ???+success "Bien configurer Java sous Linux"

            Après avoir installé la JDK 11, qui doit être dans le dossier 📂`/usr/lib/jvm/jdk-11`, pour que ce soit cette version qui soit celle par défaut, il suffit d'ajouter les lignes suivantes (à adapter) à la fin de son 📄`.profile` :

            ``` title=".profile"
            export JAVA_HOME=/usr/lib/jvm/jdk-11
            export JRE_HOME=$JAVA_HOME
            export PATH="$JAVA_HOME/bin:$PATH"
            ```
            Enfin, ouvre un terminal et exécute la commande `java -version`

            Si tu obtiens quelque chose qui ressemble à ça :

            <center>
            ![Java Version](./java-version-11-linux.gif)
            </center>

            c'est bon signe !

1. La dernière version de [**Maven**](https://maven.apache.org/download.cgi){target=_blank} (3.X à ce jour).

    À nouveau, si besoin, il faut rajouter le dossier contenant l'exécutable `mvn` dans votre `PATH`. Pour savoir s'il y a besoin de le faire, il suffit d'exécuter la commande `mvn` dans un terminal et de voir si l'exécutable est trouvé ou pas.

1. La version 10 de **tomcat**.

    Télécharge la dernière version (la 10.1.18 actuellement) (disponible ici : [https://tomcat.apache.org/download-10.cgi](https://tomcat.apache.org/download-10.cgi){target=_blank}).

    Il suffit de télécharger le zip, et de le dézipper dans le sous-dossier 📂`serveurs` dossier 📂`JEE`.

    ???+success "Le serveur tomcat"

        Tomcat va nous permettre de simuler un serveur, sur notre propre machine.

        Ainsi, en reprenant le schéma du cours, nous aurons le client (notre navigateur) et le serveur (tomcat) sur la même machine : notre PC.

        <center>![Architecture client-serveur](../images/archi-client-serveur.png)</center>



## 2. Configuration de l'environnement de développement

1. On commence par lancer **vscode**, en sélectionnant si besoin un espace de travail (_workspace_), par exemple  📂`JEE/ws_archisi`.

    ???+note "Remarque"

        On pourra placer tous les projets que nous allons créer dans ce cours dans ce _workspace_. Ils auront tous un nom différent, donc cela ne posera pas de problème.

1. Pour pouvoir faire du développement JEE, il y a un certain nombre d'extensions à installer :

    ???+success "Les extensions à installer"

        - Language support for Java - de *redhat*.
        - Debugger for Java - de *microsoft* (ou de *vscjava* sur codium).
        - Maven for Java - de *microsoft* (ou de *vscjava* sur codium).
        - Project Manager for Java - de *microsoft* (ou de *vscjava* sur codium).
        - Community Server Connectors (pour gérer un serveur Tomcat) - de *redhat*.
        - Server Connector (pour gérer un serveur WilfFly, que nous utiliserons un peu plus tard ...) - de *redhat*.

1. Il faut maintenant créer notre serveur tomcat.

    <div id="creation-serveur"/>
    ???+success "Création et démarrage du serveur Tomcat"
        Pour cela, dans la vue **SERVERS**, dans la partie **Community Server Connector**, qui est apparu lors de l'installation de l'extension du même nom,

        1. faire un clic droit et cliquer sur **Create New Server...**;
        1. Sélectionner **No, use server on disk** puisque nous venons de le télécharger.
        1. Sélectionner le dossier 📂`JEE/servers/apache-tomcat-10-x-.y`,
        1. puis cliquer sur **Finish**.

        Tu peux maintenant démarrer ce serveur, en vérifiant dans les logs qu'il n'y a pas d'erreur.

        La page d'accueil du serveur est disponible à cette adresse : [http://localhost:8080/](http://localhost:8080/){target=_blank}. Tu dois notamment y trouver une image de chat :

        <center>
            [![Tomcat](../images/Apache_Tomcat_logo.svg){width=20%}](http://localhost:8080/){target=_blank}
        </center>

Et normalement, c'est bon !!