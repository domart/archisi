---
author: Benoît Domart
title: TP noté
---

# TP noté

## 1. Les 2 APIs Web que nous allons utiliser ici

???+example "Utilisation d'API Web"

    Notre application va être cliente de deux APIs REST :

    1. L'API ***Open Library***, disponible [ici](https://openlibrary.org/developers/api){target=_blank}.

        On accède à cet API via une requête du type
        
        [https://openlibrary.org/search.json?q=Le%20Seigneur%20des%20Anneaux%20-%20tolkien](https://openlibrary.org/search.json?q=Le%20Seigneur%20des%20Anneaux%20-%20tolkien){target=_blank}.

        Il s'agit donc de modifier uniquement la valeur du paramètre de requête `q`.
    2. L'API ***Exchange Rate***, disponible [ici](https://www.exchangerate-api.com/){target=_blank}.

        On accède à cet API via une URL de ce type : [https://v6.exchangerate-api.com/v6/{API_KEY}/latest/EUR](https://v6.exchangerate-api.com/v6/{API_KEY}/latest/EUR){target=_blank}.

        ⚠️⚠️ **Attention** !
        
        1. Il faut récupérer une clef ! <red>**Faites cela en priorité !**</red>
        2. Il serait clairement préférable que cette clef soit stockée dans un fichier de propriété.

    
## 2. Consommation des API Web

L'objectif de cet exercice est d'enrichir les informations sur les livres affichés sur la page d'accueil ([http://localhost:8080/bookshop/](http://localhost:8080/bookshop/){target=_blank}), avec des informations récupérées via les deux APIs Web présentées :

- Le nombre de notes.
- La moyenne de ces notes.
- Une photo de l'auteur.
- La première phrase du livre.
- Le prix en euro du livre.

???+exercice "Exercice 1 - Consommation de l'API Open Library"

    On pourra effectuer l'appel de cette API en concaténant les champs "titre" et "auteur". Si plusieurs résultats sont renvoyés, on récupérera celui qui correspond bien à l'auteur (champ `author_name`) et au titre (champ `title`). C'est la plupart du temps le premier, mais ce n'est pas tout le temps le cas !
    
    Il faut bien sûr gérer le cas où aucun résultat n'est renvoyé.

    Voici les informations à récupérer via l'API ***Open Library***.:

    - Afficher le nombre de notes (`ratings_count`).
    - Afficher la moyenne des notes (`ratings_average`).
    - Ajouter une photo de l'auteur. Pour cela, il faut récupérer la valeur du champ `author_key`, et récupérer l'image, si elle existe, présente à cette adresse : [https://covers.openlibrary.org/a/olid/{author_key}.jpg](https://covers.openlibrary.org/a/olid/{author_key}.jpg){target=_blank}.

    ⚠️⚠️ **Attention** ! Ces informations ne doivent pas être stockées en base de données. La table `BOOK` ne doit pas contenir de champs correspondant à ces informations.

    La première phrase du livre (`first_sentence`) doit également être récupérée, mais elle ne doit pas être affichée sur la page d'accueil. Elle le sera uniquement dans la partie REST.

???+exercice "Exercice 2 - Consommation de l'API Exchange Rate"

    Les prix indiqués sont dans différentes devises (euro, franc suisse, livre, ...).

    - Rajouter un champ "prix" dans la page d'accueil indiquant les prix en euro. Ils seront arrondis au centime d'euro.


## 3. Création d'une API Rest

???+exercice "Exercice 3 - Création d'une API Rest"

    Créer une API permettant d'accéder à distance aux différents services fourni par l'application :

    1. Récupération de tous les livres.

        Il est possible d'effectuer deux filtres :

        - Soit sur l'auteur.
        - Soit sur le titre.

        Dans les deux cas, on fera une recherche "contains" en ignorant la casse. Par exemple, la recherche de "barja" comme auteur renverra tous les livres écrits par "René Barjavel".

    1. Récupération d'un seul livre.
    2. Mise à jour partielle et totale des informations (celles qui sont stockées en base uniquement) d'un livre en particulier.
    1. Mise à jour totale des informations (celles qui sont stockées en base uniquement) d'un livre en particulier.
    1. Création d'un livre.
    1. Suppression d'un livre.

    ???+tip "Concernant l'accès en lecture aux données"

        Voici les champs qu'il faudra afficher :

        - Titre
        - Auteur
        - Nombre de notes
        - Moyenne des notes
        - La première phrase du livre
        - Le prix en euro du livre
    
    ???+tip "Concernant l'accès en écriture aux données (mise à jour, création et suppression)"
    
        L'utilisateur doit être "authentifié" (comme nous avons fait dans le dernier TP, avec la valeur 42).

        Concernant la création ou la mise à jour, seuls les informations stockées dans la base de données peuvent être indiquées.

    La mise en place de cette API devra bien sûr respecter les différentes conventions dont nous avons parlé dans ce cours.

    ⚠️⚠️ Cette API ne traitera et ne renverra des données que représentées en XML ou en JSON. Les autres formats ne seront pas accepté et une erreur correspondante sera envoyée.

    Vous rendrez un document précisant les différentes URLs (*endpoint*) mises en place et comment accéder aux différents services.

