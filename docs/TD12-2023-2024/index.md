# TD12 - TP noté REST


Présentation du [projet noté](./1-tp-note.md), à rendre par groupe de 2.

## À faire avant de commencer

???success "Correction des TD10 et TD11"

    Vous trouverez ici le projet Web correction des TD10 et TD11 :

    <center>
        [Correction location (TD10 et TD11)](https://scm.univ-tours.fr/domart/location_correction){ .md-button target=_blank }
    </center>

???+success "À faire avant de commencer"

    Vous trouverez ici un projet Web présentant une application d'une librairie :

    <center>
        [bookshop](https://scm.univ-tours.fr/domart/bookshop){ .md-button target=_blank }
    </center>

    Pour le faire fonctionner, il faut avoir une JDK 11, un serveur WildFly 30.0 et importer ce projet dans votre IDE.

    Il faut également avoir une base de données nommée ***tp_rest_db*** :

    ```sql title=""
    create database tp_rest_db;
    ```

    Pour créer la *datasource* sur le serveur WildFly, vous pouvez :
    
    - L'ajouter depuis la console d'administration du serveur, comme ce que nous avons toujours fait.
    - Ou plus simplement en ajoutant au bon endroit le code suivant, dans le fichier 📄`standalone.xml`, ***lorsque le serveur est éteint***.

        Pour accéder à ce fichier, il suffit, dans VSCode (ou VSCodium), de faire un clic droit sur le serveur, `Server Actions...` > `Edit Configuration File...`.

        Ce fichier contient une balise `<datasources>` qui contient elle même des parties *Drivers* (qui a été complétée lorsque nous avons ajouté le pilote mysql), et des parties `<datasource>` (où vous retrouverez toutes les sources de données que vous avez crées).

        Il faut donc ajouter le code suivant avec les autres `<datasource>`, en adaptant bien sûr le port, le login et le mot de passe


        ```xml title="📄standalone.xml"
        <datasource jndi-name="java:/TpRestDB" pool-name="TpRestDB">
            <connection-url>jdbc:mysql://localhost:3306/tp_rest_db</connection-url>
            <driver-class>com.mysql.cj.jdbc.Driver</driver-class>
            <driver>mysql</driver>
            <security user-name="root" password="pwd"/><!--(1)!-->
            <validation>
                <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"/>
                <validate-on-match>true</validate-on-match>
                <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"/>
            </validation>
        </datasource>
        ```

        1. Si vous n'avez pas de mot de passe pour vous connecter, il faut supprimer l'attribut `password`, c'est-à-dire indiquer

            ```xml title=""
            <security user-name="root"/>
            ```

    Il faut ensuite créer le livrable (WAR) avec maven :
    ``` title=""
    mvn clean install
    ```
    et le déployer sur le serveur WildFly.