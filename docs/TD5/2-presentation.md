---
author: Benoît Domart
title: Présentation JDBC
---

# Présentation - Utilisation de JDBC

JDBC (pour _Java DataBase Connectivity_) est une API (pour _Application Programming Interface_ - interface de programmation) qui permet d'effectuer une connexion à une base de données depuis une application Java.

Dans cette première partie, nous allons implémenter nous même la couche d'accès aux données (couche _DAO_, pour _Data Access Object_). Dans le prochain TD, nous verrons comment utiliser une API pour faire cela (_JPA_, pour _Java Persistence API_). Nous allons donc pour l'instant mettre en place l'architecture suivante :

<figure markdown>
  ![Architecture TD3 - Exercice 1](../images/schema_archi_V1.png)
  <figcaption>Schéma de l'architechture du TD5 (JDBC)</figcaption>
</figure>
