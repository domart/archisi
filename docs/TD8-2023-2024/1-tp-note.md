---
author: Benoît Domart
title: e-commerce
---

# TP noté - Site d'un site de commerce en ligne

## 1. Objectif

???+success "Objectif"
    Modéliser et implémenter une application Web permettant de gérer un site de commerce en ligne.

## 2. Cahier des charges

???+cdc "Cahier des charges"
    Cette application contient trois pages. Les différentes **règles de gestion** (RG) à mettre en place sont listées ici.
    
    1. Une page de connexion à l'application (nommée `PAGE1`), contenant un champ *login* et un champ *mot de passe*.

        ???+example "RG1"
        
            L'application vérifie que cet utilisateur est présent dans la table `USER` et qu'il a saisit le bon mot de passe.
            
            - Si l'une ou l'autre de ces conditions n'est pas remplie, l'utilisateur reste sur cette page de connexion (`PAGE1`) et le message

                <red>Le nom d'utilisateur ou le mot de passe saisit n'est pas valide.</red>

                est affiché en rouge.
            
            - Si les deux conditions sont remplies, l'utilisateur est redirigé vers la page listant les articles disponibles (`PAGE2`).
        
        ???+example "RG2"

            Si une URL quelconque est saisie par un utilisateur qui n'est pas connecté, il est automatiquement redirigé vers cette page de connexion (`PAGE1`).
        
        <center>
            ![type:video](./videos/connexion.gif){width=70%}
        </center>

        ***Bonus*** : Si vous avez le temps, vous pouvez ne pas stocker le mot de passe en base en clair (**ce qu'il ne faut bien sûr jamais faire !**). Pour cela vous pouvez vous inspirer de [cette page](../3-md5).

    1. Une page listant les articles présents en base de données (`PAGE2`).

        ???+example "RG3"

            Tous les articles présents en base de données dans la table `ARTICLE` sont affichés sous la forme d'une liste.

            Pour chaque article, son nom, son prix et le nombre en stock sont affichés. De plus, pour chaque article, un lien `-` (pour enlever un exemplaire de cet élément du panier) et un lien `+` (pour ajouter un exemplaire de cet élément du panier) sont également affichés.

            Un bouton permet d'accéder au contenu du panier (`PAGE3`).
        
        ???+example "RG4"

            Lorsque l'utilisateur clic sur le bouton `+` :

            - L'application vérifie qu'il reste au moins un exemplaire disponible de cet article dans la base de données.

                - Si c'est le cas, la base de données est mise à jour pour indiquer que cet article est réservé, et un exemplaire est placé dans le panier de l'utilisateur connecté.
                - Si ce n'est pas le cas, le clic n'a aucun effet (aucun message n'est indiqué à l'utilisateur).
            
            - Dans tous les cas, l'utilisateur reste sur cette page listant les articles (`PAGE2`).
        
        ???+example "RG5"

            Lorsque l'utilisateur clic sur le bouton `-` :

            - L'application vérifie qu'il y a au moins un exemplaire de cet article dans le panier de l'utilisateur connecté.

                - Si c'est le cas, un exemplaire de cet article est supprimé du panier, et le nombre d'exemplaire disponibles en base de données est augmenté de 1.
                - Si ce n'est pas le cas, le clic n'a aucun effet (aucun message n'est indiqué à l'utilisateur).
            
            - Dans tous les cas, l'utilisateur reste sur cette page listant les articles (`PAGE2`).
        
        <center>
            ![type:video](./videos/ajout-suppression.gif){width=70%}
        </center>

    1. Une page affichant le contenu du panier de l'utilisateur (`PAGE3`), accessible depuis un bouton <bouton>Afficher la panier</bouton> sur la page listant les articles présents dans la base de données.

        ???+example "RG6"

            Cette page liste tous les articles présents dans le panier de l'utilisateur connecté, avec leur prix unitaire et le nombre d'exemplaires commandés.

            Cette page contient également le prix total de cette commande.

            Remarque : La liste peut contenir des articles avec 0 exemplaires (pour simplifier le cas où l'utilisateur a sélectionné un article puis l'a supprimé de son panier).
        
            <center>
                ![type:video](./videos/panier.gif){width=70%}
            </center>
    
    4. Sur la page listant les articles (`PAGE2`) et sur le contenu du panier (`PAGE3`) :
    
        ???+example "RG7"

            Un bouton *Déconnexion* est présent qui a pour effet de clore la session de l'utilisateur.

            Il est alors redirigé vers la page de connexion (`PAGE1`) et doit à nouveau s'authentifier s'il souhaite accéder à l'application.

            Le clic sur le bouton a également pour effet de vider le panier de l'utilisateur (et donc de remettre les articles éventuellement sélectionnés comme disponibles dans la base de données).
        
        ???+example "RG8"

            À côté de ce bouton *Déconnexion*, le nom de l'utilisateur connecté est affiché.
        
        <center>
            ![type:video](./videos/disonnect.gif){width=70%}
        </center>

## 3. Livrables

???+success "Livrables"

    Merci de fournir le lien vers le dépôt git contenant vos sources (attention à vérifier que l'accès est bien public).

    **Il sera vérifié que les deux membres du binômes ont bien effectué des commits.**

    **Conseil :** Je vous conseille de commencer par une architecture semblable à celle du TD6 (c'est-à-dire un unique projet WAR). Une fois que tout est fonctionnel, en dernier lieu, vous pourrez passer à une architecture semblable à celle du TD7 (un projet parent, un projet EAR, un projet WAR, et un projet JAR - EJB). **Cette seconde architecture vous rapportera plus de points**, mais ne doit être mise en place que si tout le reste est fonctionnel.

    **Si besoin**, vous pouvez livrer une petite documentation (éventuellement très rapide). Je ne suis pas dans votre tête. S'il faut exécuter un script pour que votre application soit fonctionnelle, il faut me le préciser !!

## 4. Le modèle de données

???+success "Le modèle de données"

    La base de données ne contient que 2 tables : `USER` et `ARTICLE`.

    ???abstract "Script SQL"

        ```sql
        create schema boutique;

        insert into boutique.ARTICLE (name, prix, nbRestant) values ("Table", 200, 2);
        insert into boutique.ARTICLE (name, prix, nbRestant) values ("Chaises", 50, 10);
        insert into boutique.ARTICLE (name, prix, nbRestant) values ("Banc", 100, 5);

        insert into boutique.USER (login, name, password) values ('alice', 'Alice', 'mdpa');
        insert into boutique.USER (login, name, password) values ('bob', 'Bob', 'mdpb');
        ```

    Les deux beans correspondants sont fournis ici :

    ???abstract "Code de `UserBean`"

        ```java
        import java.io.Serializable;

        import jakarta.persistence.Column;
        import jakarta.persistence.Entity;
        import jakarta.persistence.Id;
        import jakarta.persistence.Table;

        @Entity
        @Table(name = "USER")
        public class UserBean implements Serializable {

            @Id
            @Column(name = "LOGIN")
            private String login;
            private String name;
            private String password;

            public String getLogin() {
                return login;
            }

            public void setLogin(String login) {
                this.login = login;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }
        }
        ```
    
    ???abstract "Code de `ArticleBean`"

        ```java
        import java.io.Serializable;

        import jakarta.persistence.Entity;
        import jakarta.persistence.GeneratedValue;
        import jakarta.persistence.GenerationType;
        import jakarta.persistence.Id;

        @Entity(name = "ARTICLE")
        public class ArticleBean implements Serializable {

            @Id
            @GeneratedValue(strategy = GenerationType.IDENTITY)
            private Integer id;
            private String name;
            private Float price;
            private Integer nbRestant;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Float getPrice() {
                return price;
            }

            public void setPrice(Float price) {
                this.price = price;
            }

            public Integer getNbRestant() {
                return nbRestant;
            }

            public void setNbRestant(Integer nbRestant) {
                this.nbRestant = nbRestant;
            }
        }
        ```

    **Toutes les les autres informations ne sont pas stockées en base de données.**

## 5. Conseils

???+tip "Conseils"

    Avant de vous lancer dans le développement, il faut réfléchir un peu.

    1. Concernant la conception :

        - Faite un schéma des différents écrans et des actions que l'utilisateur peut effectuer.
        - Vous en déduirez les différentes *servlets* à créer.
        - Listez les différents services offerts par l'application.
        - Réfléchissez à votre modèle. Il y a certainement d'autres *beans* qu'il faudra créer, même s'ils ne sont pas stockés en base de données.

        Ensuite, essayez de vous répartir le travail de façon à ne pas être trop dépendant de votre binôme ...

    2. Concernant le développement :

        - Commencez par développer le modèle,
        - Puis les DAO,
        - Puis les services métiers,
        - Puis les contrôleurs,
        - Et terminer enfin par les vues.


???+tip "Conseils - Suite"

    N'oubliez pas de mettre dans le `persistence.xml` l'option `drop and create`, au moins la première fois, pour que les tables soient créées dans la BDD.

    Une fois que votre modèle est stable (la partie "persistée" ne devrait de toutes façon pas changer), vous pourrez éventuellement supprimer cette option (mettre `Default (None)` à la place - la valeur par défaut)0
    
    Je vous conseille plutôt de ré-exécuter le script d'insertion de données (fourni ci-dessus - que vous pourrez étoffer) à chaque publication de l'application.

## 5. Qu'est ce qui sera évalué ?
???+tip "Qu'est ce qui sera évalué ?"

    Les points suivants seront (entre autre) évalués :

    - Architecture :
        - Le code est bien découpé selon les briques vue dans ce cours : contrôleur, vue, couche modèle, couche services métiers, couche dao, ...
        - Chaque brique est bien responsable de ce qu'elle doit faire.
        - Chaque brique communique bien uniquement avec les briques avec lesquelles elle doit communiquer.
    
    - Code fonctionnel :
        - L'application **fonctionne** !
        - Les éventuels bugs (c'est normal qu'il y ait des bugs dans un logiciel) sont listés dans la doc.
        - Les fonctionnalités non développées sont listées dans la doc.
    
    - Qualité du code :
        - Le code est bien formaté.
        - Il n'y a aucun warning (notamment aucun import en trop).
            *Remarque* : Utiliser des `@SuppressWarnings("unchecked")` est accepté uniquement dans les DAO, lors de l'appel de `Query.getResultList()` par exemple.
        - Les classes sont bien nommées (CamelCase et indication de la brique dans le nom de la classe).
        - Les variables utilisées ont des noms explicites.