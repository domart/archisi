## 1. Rappel sur l'architecture client/serveur

Une architecture client serveur est constituée de 2 programmes et d'un protocole :

1. Un programme client qui effectue une requête (il pose une question).
2. Un programme serveur qui propose des services (il effectue des traitements pour répondre à cette question).
3. Un protocole (c'est la façon dont les deux programmes vont communiquer).

<figure markdown>
  ![archi td4](../images/archi-client-serveur.png)
  <figcaption>Architecture client serveur</figcaption>
</figure>

???+ note "Pour les Servlets"

    Dans le cas des servlets, on avait :

    1. Le programme client est le navigateur.
    2. Le programme serveur est la _Servlet_.
    3. Le protocole de communication est HTTP.

???+ note "Pour les _EJB_"

    Dans le cas des _EJB_, nous allons avoir :

    1. Le programme client est le client lourd Java ou la couche "présentation" (i.e. la _Servlet_).
    2. Le programme serveur est l'_EJB_.
    3. Le protocole de communication est RMI, pour _Remote Method Invocation_ (_Appel de Méthodes à Distance_ en français) (nécessaire en fait uniquement si le client et le serveur sont sur des JVM différentes).

## 2. Les _EJBs_

**Présentation**

De même que le cycle de vie des _Servlets_ (naissance / attente de requête HTTP / destruction) était géré par le **conteneur de Servlets** disponible sur le serveur Web, le cycle de vie des _EJBs_ est géré par un conteneur d'_EJB_.

??? note "Tomcat et WildFly"

    À nouveau, Tomcat fourni uniquement un conteneur de _Servlets_. Pour utiliser des _EJBs_, il faut obligatoirement utiliser un serveur d'application. Nous allons à nouveau utiliser WildFly.

<!--
??? note

    Toutes les annotations utilisées ici sont dans le package `javax.ejb`.
-->

??? note "Les _EJBs_"

    Il existe trois types d'_EJBs_ :

    1. Les **_EJB_ entités** : C'est exactement ce que nous avons utilisé, sans le dire, avec les entités JPA (ce sont des _beans_ classiques).<br>
        On utilise l'annotation `@Entity` pour les définir (ce que nous avons fait dans le TD précédent).
    2. Les **_EJB_ sessions** : C'est eux qui vous nous permettre d'appeler les services métiers depuis l'extérieur (c'est en fait de ces _EJBs_ dont on parle depuis le début de ce TD).<br>
        On utilise 3 annotations différentes pour les définir - `@Singleton`, `@Stateless` et `@Statefull` - Cf. ci-dessous.
    3. Les **_EJB_ messages** : Nous n'allons pas les utiliser ici. Ils permettent d'appeler des traitements asynchrones.

???+ note "Les **_EJBs_ sessions**"

    Il y trois possibilités :

    - `@Singleton` : Cela signifie qu'il n'y a qu'un seule instance de cette classe.
        Tous les clients effectuant des requêtes partagent donc la même.
    - `@Stateless` : Cela signifie que les échanges (c'est-à-dire les requêtes) se feront sans état, c'est-à-dire sans mémoire.
        Cela ressemble beaucoup à `@Singleton`, mais la différence est que le serveur peut décider de créer de nouvelles instances de la classe s'il y a trop de clients en même temps.
    - `@Statefull` : Cela signifie que les échanges se feront avec mémoire. L'historique des requêtes effectuées par un client est conservé.
        Concrétement, chaque client dispose de sa propre instance de la classe. On peut donc avoir des variables de classe pour conserver des données en mémoire entre les requêtes.

**Cycle de vie**

Voici à quoi ressemble le cycle de vie d'un _EJB_ session, de la manière dont nous allons l'utiliser :

``` mermaid
sequenceDiagram
    participant Client (Servlet)
    participant Conteneur d'EJB
    participant EJB Session
    participant Service de transaction (JTA)
    Client (Servlet) ->> Conteneur d'EJB: 1. Appel
    Conteneur d'EJB  ->> Service de transaction (JTA): 2. begin Transaction
    Conteneur d'EJB  ->> EJB Session: 3. Appel
    EJB Session  ->> EJB Session: 4. Traitement
    EJB Session  -->> Conteneur d'EJB: 5. Résultat ou Exception
    Conteneur d'EJB  ->> Service de transaction (JTA): 6. commit ou rollback
    Conteneur d'EJB  -->> Client (Servlet): 7. Résultat ou Exception
```

Par rapport à ce diagramme, il nous suffit d'implémenter le point 4 : on appelle le service _DAO_ qui convient. Tout le reste est géré par le framework.

??? note "Ce que nous avons fait dans le TD3"

    Ce que nous avons fait précédemment ressemblait beaucoup à cela, mais nous devions gérer les transaction en plus. Nous avions en fait implémenté ce cycle de vie :

    ``` mermaid
    sequenceDiagram
        participant Client (Servlet)
        participant Business et DAO
        participant Service de transaction (JTA)
        Client (Servlet) ->> Business et DAO: 1. Appel
        Business et DAO  ->> Service de transaction (JTA): 2. begin Transaction
        Business et DAO  ->> Business et DAO: 3. Traitement
        Business et DAO  ->> Service de transaction (JTA): 4. commit ou rollback
        Business et DAO  -->> Client (Servlet): 5. Résultat ou Exception
    ```

    Dans ce diagramme, il fallait implémenter tous les points :

    1. Instanciation de l'objet `NoteBusinessImpl` depuis la méthode `init` des Servlets.
    2. Exécution de `em.getTransaction().begin();` depuis le _DAO_.
    3. Exécution de la requête (avec `persist` par exemple).
    4. Exécution de `em.getTransaction().commit();` ou `em.getTransaction().rollback();` depuis le _DAO_.


???+success "Création d'un _EJB_ session"

    Concrètement, un _EJB_ est une classe qui implémente **deux interfaces** :

    - **Une interface locale**, qui liste les services disponibles pour un appel local de l'_EJB_ (i.e. depuis la même JVM).<br>
        On utilise l'annotation `@Local`.
    - **Une interface distante**, qui liste les services disponibles pour un appel à distance de l'_EJB_ (i.e. depuis une autre JVM, éventuellement sur un autre serveur).
        On utilise l'annotation `@Remote`.

    On peut donc très facilement choisir de ne pas déployer les mêmes services pour les accès en local ou à distance, alors qu'il n'y a qu'une seule implémentation.
