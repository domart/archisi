---
author: Benoît Domart
title: Exercice 1 - Implémentation de la partie métier
---

# Exercice 1 - Implémentation de la partie métier

Dans cet exercice, nous allons implémenter la partie "Couche Métier" du schéma suivant :

<figure markdown>
  ![archi appli](../images/schema_archi_V3.png)
  <figcaption>Architecture de notre application</figcaption>
</figure>

## 1. Création du projet _EJB_

Nous allons créer le projet qui va permettre d'implémenter la couche métier. Comme indiqué précédemment, pour pouvoir contenir des _EJBs_ Sessions, le projet doit être du type _EJB_.

1. Dans le vue _Project Explorer_, cliquer sur le bouton droit de la souris, puis aller dans :material-mouse:`New > Project...`, puis dans :material-file-tree:`EJB > EJB Project`.
2. Entrer le nom du projet (**_GestionNotesEJB_**), puis :

    - sélectionner la bonne _runtime_,
    - décocher "Add Project to an EAR" si elle est cochée,
    - dans le dernier onglet, cocher la case "Generate ejb-jar.xml ..." (c'est l'équivalent du 📄`web.xml`)
    - puis cliquer sur <bouton><u>F</u>inish</bouton>.

3. Pour pouvoir utiliser _JPA_, il faut l'activer dans les "facettes" du projet.

## 2. Implémentation des différentes couches

Ce projet contient trois sous-couches :

1. La couche "modèle".
2. La couche _DAO_.
3. La couche "Services métiers" (la brique "_EJB_ Session" dans le schéma ci-dessus).

Les modifications décrites ci-dessous le sont par rapport au projet précédent (**_GestionNotes_** du TD6).

### 2.1. Implémentation de la couche "modèle"

La couche "modèle" est inchangée. Il faut la recréer à l'identique.

Il ne faut pas oublier de configurer le fichier 📄`persistence.xml`, comme dans le TD précédent.

### 2.2. Implémentation de la couche "DAO"

La couche _DAO_ est modifiée :

1. Le constructeur public sans argument, qui permettait d'instancier l'objet `EntityManager` doit être supprimé.
2. Cet objet est maintenant instancié via injection, en utilisant l'annotation suivante :

    ``` java
    @PersistenceContext(unitName = "GestionNotesEJB")//(1)!
    private EntityManager em;
    ```

    1. Le nom de l'unité de persistance doit être celui indiqué dans le fichier 📄`persistence.xml`

3. Dans les méthodes gérant de la persistance (les CUD dans [CRUD](https://fr.wikipedia.org/wiki/CRUD)) (il n'y a normalement que la méthode `insertNote`), les gestions manuelles de la transaction sont à supprimer : `em.getTransaction().begin();` et `em.getTransaction().rollback();`.
4. L'appel de la couche _DAO_ (depuis la couche "services métiers") se fera sans état. Il faut donc rajouter l'annotation suivante à la création de la classe :

    ``` java
    @Stateless
    public class NoteDAOImplJPA implements NoteDAO {
        ...
    }
    ```
5. L'implémentation *DAO* *JDBC*, c'est-à-dire la classe `NoteDAOImplJDBC` peut être supprimée.

### 2.3. Implémentation de la couche "Services métiers"

La couche "Services métiers" est également modifiée :

1. Comme indiqué précédemment, un _EJB_ session est une classe implémentant deux interfaces (une pour les appels locaux et une pour les appels distants).

    1. Renommer l'interface `NoteBusiness` en `NoteBusinessLocal`. Pour indiquer qu'il s'agit de la locale, il faut ajouter l'annotation suivante :

        ``` java
        @Local
        public interface NoteBusinessLocal {
            ...
        }
        ```
    2. Créer l'interface `NoteBusinessRemote`. Pour indiquer qu'il s'agit de la distante, il faut ajouter l'annotation suivante :

        ``` java
        @Remote
        public interface NoteBusinessRemote {
            ...
        }
        ```

        Cette interface contient exactement les mêmes signatures que la locale.
    
    3. Dans la classe `NoteBusinessImpl`, il faut maintenant indiquer qu'elle implémente les **deux** interfaces. Il faut également indiquer qu'elle sera appellée "sans état" depuis la _Servlet_. On ajoute donc l'annotation `@Stateless`. On obtient donc :

        ``` java
        @Stateless
        public class NoteBusinessImpl implements NoteBusinessLocal, NoteBusinessRemote {
            ...
        }
        ```
    4. Enfin, le _DAO_ n'est maintenant plus instancié "à la main", mais par injection. Le constructeur public doit donc être supprimé, et remplacé par l'utilisation de l'annotation suivante :

        ``` java
        @Inject
        private NoteDAO dao;
        ```


## 3. Déploiement du projet

Déployer le projet **_GestionNotesEJB_** sur le serveur WildFly.

???+ success "Vérifier que l'_EJB_ session est correctement déployé"

    Vérifier dans les logs que l'_EJB_ session a bien été déployé. Il doit y avoir une ligne ressemblant à cela :

    ``` title="📋 Logs du serveur WildFly"
    ejb:/GestionNotesEJB/NoteBusinessImpl!fr.univtours.polytech.gestionnotes.business.NoteBusinessRemote
    ```

    Il s'agit du nom JNDI (c'est annuaire des objets Java disponibles) avec lequel nous allons pouvoir appeler l'_EJB_ session depuis une autre application Java.

    Ces noms n'ont pas la même forme selon le serveur d'application utilisé. Pour WildFly, la forme est la suivante :

    ``` title="Détail du nom JNDI"
    ejb:<Nom du projet d'entreprise>/<Nom du projet EJB>/<Nom de la classe implémentant l'EJB session>!<Package et nom de l'interface remote>
    ```

    Ici, notre module _EJB_ (le projet que nous venons de créer) n'est pas encore associé à un projet d'entreprise, c'est pour cela qu'il est vide.

???error "java.lang.UnsupportedClassVersionError"

    Si vous rencontrez l'erreur `java.lang.UnsupportedClassVersionError`, c'est qu'une classe est exécutée avec une version de Java avec laquelle elle n'a pas été compilée.

    Cela vient du fait que la JDK 1.8 n'a pas été indiqué partout.

    L'action suivante corrigera certainement ce problème :
    
    - Faire un clic droit sur le projet ***GestionNotesEJB***, :material-mouse:`Properties`, puis dans :material-file-tree:`Java Build Path`, dans l'onglet `Libraries`, cliquer sur la JDK ou JRE utilisée, puis sur `Edit...` et sélectionner une JDK 1.8.
    - Dans :material-file-tree:`Java > Compiler`, puis dans le champ `Compiler compliance level`, indiquer `1.8`.
    - Dans les "facettes" du projet, indiquer que la version de Java utilisée est la 1.8.

??? note

    Plusieurs projets peuvent être déployés en même temps sur le même serveur (Tomcat, WildFly, ou autre). Il n'est donc pas nécessaire de dépublier (le contraire de déployer) les anciens projets.

    Cependant, pour s'y retrouver dans les logs, il est fortement conseillé de ne déployer que les projets nécessaires au développements en cours.


Ça y est, notre service _EJB_ est accessible depuis une autre application. Nous allons tout de suite le tester en créer un client (lourd) Java dans le prochain exercice.
