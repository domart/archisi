---
author: Benoît Domart
title: Les Web Services
---

# Les Web services - Architecture cible

L'objectif est ici de réussir à faire communiquer différentes applications entre elle, alors qu'elles ne sont pas nécessairement écrites dans les mêmes technologies.

<center>![archi ws](../images/web-services-intro.png)</center>

<center>![Archi appli Web](../images/schema_archi_V5.png)</center>
