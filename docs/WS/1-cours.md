---
author: Benoît Domart
title: Cours
---

# Les Web services - Cours

## 1. Web Services et Web API

Qu'entend-on par Web Service ?

- Il correspond à un périmètre fonctionnel (services métiers ou dao) que l'on souhaite exposer à des consommateurs.
- Il est faiblement couplé : il est indépendant des autres services.
- Il est sans état.

Le terme de **Web service** est problématique car, pour des raisons historiques, il peut regrouper plusieurs acceptions. Dans ce cours, nous en distinguerons deux, que nous essaierons de nommer ainsi :

1. Un **Service Web** : Protocole de communication basé sur des standards tels que XML, SOAP et WSDL.
2. Une **API Web** (parfois nommée *RESTful Web Services*) : interface programmatique basée sur HTTP.

Ici, nous parlerons rapidement de la première, et nous nous attarderons sur la seconde.

Dans les deux cas, **l'objectif est de faire communiquer deux applications distantes, de manière indépendante des technologies utilisées pour développer ces deux applications**.


## 2. Les Web Services

Les Web Services sont basés sur 3 concepts :

1. *SOAP (Simple Object Access Protocol)*

    - Recommandation pour l'échange inter-applications indépendant de toute plateforme basé sur le langage XML
    - Un appel de service SOAP est un flux ASCII encadré dans des balises XML et transporté par un protocole (presque tout le temps HTTP)

2. *WSDL (Web Services Description Language)*

    - Description des WS offerts, en précisant les signatures des méthodes

3. *UDDI (Universal Description, Discovery and Integration)*

    - Annuaire de WS, permettant la publication et la recherche
    - C’est un WS

<center>![Concepts WS](../images/web-services-concepts.png)</center>

### 2.1. SOAP

SOAP est une recommandation pour l'échange de messages au format XML entre un expéditeur (*SOAP sender*) et un destinataire (*SOAP receiver*). La structure des messages SOAP est identique qu'elle provienne de l'expéditeur (requête) ou du destinataire (réponse). Un message est constitué par une enveloppe (*SOAP envelope*) qui peut contenir un en-tête (*SOAP header*) et ensuite une charge utile (*SOAP body*).

La plupart du temps, la requête et la réponse sont envoyées par le protocole HTTP, mais ce n'est pas une obligation. Pour une utilisation avec le protocole HTTP, le message doit être envoyé avec la méthode POST à l'URL du service (appelée aussi *endpoint*)

<center>![SOAP](../images/web-services-soap.png)</center>

### 2.2. WSDL

La condition pour utiliser un service Web *SOAP* est de connaître les opérations disponibles et le format des messages autorisés en entrée et/ou en sortie de ses opérations. *WSDL* (*Web Service Description Language*) est un langage XML permettant la description complète d'un service Web. Ainsi un fichier *WSDL* est analysable par programme et on trouve des outils dans différents langages de programmation pour générer du code (les stubs) facilitant le développement des programmes (client ou serveur).


???exemple "Un exemple de WSDL"

    ```xml
    --8<-- "docs/WS/sources/tempconvert.asmx.xml"
    ```

???+note "En résumé"

    - *Web Service Description Language* : Format XML
    - Pour l'obtenir : `https://url.web.service?wsdl`
    - Décrit le WS, les méthodes, les paramètres d'entrée/sortie ...
    - Un peu compliqué ... plus pour les machines que pour les humains.

### 2.3. UDDI

*Universal Description Discovery and Integration*, connu aussi sous l'acronyme *UDDI*, est un annuaire de services fondé sur XML et plus particulièrement destiné aux services Web. [^1]

Nous ne nous attarderons pas sur ce point dans ce cours.

[^1]: Source : [Wikipedia](https://fr.wikipedia.org/wiki/Universal_Description_Discovery_and_Integration)

### 2.4. Tester un service Web

Faire [l'exercice 1 du TD9](../../TD9/1-exercice1/){target=_blank}.

### 2.5. Les service Web en JEE

Il y a deux approches pour développer un service Web :

1. **Contract first** : On définit le contrat, c'est-à-dire le WSDL, et on utilise un outils pour générer le code des *stubs*.
2. **Contract last** : On développe le service, et on utilise des outils pour générer le WSDL.

Dans l'exercice 2, nous allons étudier la première approche, puis la seconde dans l'exercice 3.

#### 2.5.1 - Consommer un service Web avec JAX-WS

Faire [l'exercice 2 du TD9](../../TD9/2-exercice2/){target=_blank}.

#### 2.5.2 - Implémenter un service Web avec JAX-WS

Faire [l'exercice 3 du TD9](../../TD9/3-exercice3/){target=_blank}.


<!-- [https://gayerie.dev/epsi-poe-201703/web-services/a2_ws_soap.html](https://gayerie.dev/epsi-poe-201703/web-services/a2_ws_soap.html) -->