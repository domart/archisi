---
author: Benoît Domart
title: Exercice 2 - Tests V1.0
---

# Exercice 2 - Déploiement de l'application et tests

## 1. Création d'un serveur local

Pour tester notre application, il faut la déployer sur un serveur (tomcat).

## 2. Déploiement de l'application sur ce serveur

On crée ensuite le livrable, [via le script *Maven*](../../TD1/exercice2/#creation-livrable){target=_blank}, puis on déploie l'application **_boutique_** sur le serveur, comme [vu précédemment](../../TD1/exercice2/#deploiement-application){target=_blank}.

Notre application est disponible à l'adresse [http://localhost:8080/boutique](http://localhost:8080/boutique){target=_blank} (du fait de notre modification du fichier 📄`web.xml`, cette URL pointe vers [http://localhost:8080/boutique/store](http://localhost:8080/boutique/store){target=_blank}).


## 3. Test de l'application

Lorsqu'on va ici : [http://localhost:8080/boutique/store](http://localhost:8080/boutique/store) (méthode _GET_), la page s'affiche correctement. Mais lorsqu'on clique sur <bouton>Valider</bouton> (méthode _POST_), une erreur apparaît. Plus précisément, c'est une erreur 405 qui nous précise que la méthode demandée (POST), n'est pas autorisée :
<center>
    ![Erreur méthode POST](../images/boutique_v1.0_erreur.png)
</center>

C'est normal, dans notre _servlet_, nous n'avons implémenté que la méthode `doGet`, donc les requêtes _GET_ (premier affichage de la page) sont gérées. Mais nous n'avons pas implémenté la méthode `doPost`, ce que nous allons faire maintenant ... pour gérer la soumission du formulaire par l'utilisateur.
