---
author: Benoît Domart
title: Exercice 4 - V3
---

# Exercice 4 - Version 3

## 1. Suppression du code Java dans la vue

Notre application est fonctionnelle, mais elle ne respecte pas les bonnes pratiques. En effet, il ne faut pas mettre de code Java dans la vue. Pour le supprimer, nous allons utiliser la [JSTL](https://www.jmdoudoux.fr/java/dej/chap-jstl.htm){target=_blank} : la **_JavaServer pages Standard Tag Library_**.

Plus précisément, nous allons utiliser le langage [EL](https://www.jmdoudoux.fr/java/dej/chap-jstl.htm#jstl-2){target=_blank} (pour *Expression Language*) offert par la JSTL, qui permet d'accéder aux objets Java présents dans la requête (ainsi que dans la session, entre autre).

Nous allons donc supprimer :

```jsp linenums="1"
<%@page import="fr.univtours.polytech.boutique.model.CartBean"%>
```

et

``` jsp linenums="1"
<%
CartBean cart = (CartBean) request.getAttribute("USER_CART");
%>
```

Le code suivant, qui nous permettait d'accéder aux propriétés du _bean_ :

``` jsp linenums="1"
<%=cart.getCartPrice()%>
```

va être remplacé par

``` jsp linenums="1"
${requestScope.USER_CART.cartPrice}<!--(1)!-->
```

1. Précisions :<br><br>
    `requestScope` indique que l'objet en question est présent dans la requête (on utilise `sessionScope` si l'objet est présent dans la session).<br><br>
    `USER_CART` est la clef avec laquelle l'objet a été placé dans la requête (ou dans la session).<br><br>
    `cartPrice` est le champ du bean à afficher.

???+ note "Explication sur le champ du bean"

    Concrétement, la méthode dont le nom est obtenu en mettant le premier caractère en majuscule (ce qui donne `CartPrice`) et en préfixant par `get`. Ici la méthode `getCartPrice()` (sans paramètre) est appelée.

Dans la vue 📄`store.jsp`, remplacer toutes les expressions JSP (`<%=...%>`) par des EL (`${...}`).

???+tip "Pour les informations placées en session"

    Si le `USER_CART` avait été placé en session, il aurait suffit d'écrire ${sessionScope.USER_CART....}` pour récupérer les différentes informations.


➡️ Ça y est, nous avons terminé le développement de notre première application Web respectant le paradigme MVC 💪