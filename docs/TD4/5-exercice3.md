---
author: Benoît Domart
title: Exercice 3 - V2
---

# Exercice 3 - Amélioration de l'application

### 1. Développement de la V2.0

Nous allons donc maintenant rajouter l'implémentation de la méthode `doPost` dans notre _servlet_ pour gérer cette requête :

1. Dans la classe `StoreServlet`, surcharger la méthode `doPost` de `HttpServlet`.

    ???+success "Astuce : surcharge rapide"

        Nous avons vu qu'il était possible d'ajouter la méthode `doPost` via *Source Actions...*.
        
        On peut également écrire le début de son nom, ici le début de `doPost`, et appuyer sur ++ctrl+"Entrée"++ pour faire de l'autocomplétion. On obtient le même résultat, mais plus rapidement !

        <center>
            ![](./override.gif){width=70%}
        </center>

2. Comme pour la méthode `doGet`, renommer les paramètres en `request` et `response` respectivement.
3. Dans cette méthode, nous allons récupérer les données saisies par l'utilisateur depuis le formulaire HTML. Ces données sont en paramètre de la requête HTTP, que la méthode utilisée soit _GET_ ou _POST_. Nous allons donc faire `request.getParameter(<attribut_name>)`.

    ``` java title="☕ Code Java - Méthode doPost - 1/4" linenums="1"
    String penNumber = request.getParameter("penNb");//(1)!
    String feltNumber = ...;
    String rubberNumber = ...;
    ```

    1. Le nom du paramètre, ici `penNb`, doit correspondre à l'attribut `name` de la balise `<input type="*">` du formulaire HTML.

4. Ces données saisies par l'utilisateur doivent être "envoyées" au modèle. Dans cette méthode, on rajoute donc :

    ``` java title="☕ Code Java - Méthode doPost - 2/4" linenums="1"
    CartBean cart = new CartBean();

    cart.setPenNumber(Integer.parseInt(penNumber));//(1)!
    ...
    ...
    ```

    1. Les objets récupérés en paramètre de la requête sont des `String`. Ici, il faut donc le transformer en `Integer`.

5. Ensuite, on appelle la couche métier, pour mettre à jour les données du panier (les 3 RG) en fonction de ce que l'utilisateur a sélectionné. Pour cela, il va nous falloir une instance de la classe implémentant l'interface `StoreBusiness`. On pourrait l'instancier ici, mais on aurait alors une nouvelle instance à chaque appel de la Servlet, ce qui serait inutile. On va donc instancier cette classe une seule fois, à l'initialisation de la Servlet (c'est-à-dire en gros au démarrage du serveur).

    La méthode `void init() throws ServletException` sert à cela.

    Dans cette classe `StoreServlet`, il faut donc ajouter :
    
    - Une propriété privée, qui permet de créer la dépendance vers la couche métier.
    - L'implémentation de la méthode `init`, qui permet d'instancier cette dépendance.

    ```java
    private StoreBusiness storeBusiness;

    @Override
	public void init() throws ServletException {
		this.storeBusiness = new StoreBusinessImpl();//(1)!
	}
    ```

    1. C'est ici qu'on indique quelle classe implémente notre interface.

        Pour l'instant, nous indiquons "en dur" quelle classe implémente l'interface. Nous verrons bientôt comment indiquer cela dans un fichier de configuration, afin de pouvoir changer d'implémentation **sans avoir à toucher au code**.

    Enfin, dans la méthode `doPost`, on peut maintenant appeler la couche métier :

    ``` java title="☕ Code Java - Méthode doPost - 3/4" linenums="1"
    cart = this.storeBusiness.computePrices(cart);
    ```

6. Enfin, on redirige l'utilisateur vers la bonne vue, c'est-à-dire `store.jsp` :

    ``` java title="☕ Code Java - Méthode doPost - 4/4" linenums="1"
    RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
    dispatcher.forward(request, response);
    ```

## 2. Test de la V2.0

En testant à nouveau (il suffit pour cela de relancer le script *Maven* et de redémarrer le serveur), on constate maintenant :


1. Qu'au premier affichage, le nombre de chaque article est vide, et qu'il y a une erreur si on ne met pas des nombres partout ...
2. Qu'il ne se passe rien si on saisit des nombres partout et qu'on cliquer sur <bouton>Valider</bouton> ...

## 3. Développement de la V2.1

Nous allons commencer par traiter le deuxième problème.

Le comportement observé est en fait normal. Le modèle (ici, le _bean_ `CartBean`) n'est pas "envoyé" à la vue par le contrôleur.

➡️ Pour cela, nous allons l'ajouter dans la requête. À la fin de la méthode `doPost` de la servlet, juste avant l'appel du `RequestDispatcher`, il faut ajouter :

``` java title="☕ Code Java - StoreServlet.doPost" linenums="1"
request.setAttribute("USER_CART", cart);//(1)!
```

1. `setAttribute` permet d'ajouter un **attribut** à la requête (à ne pas confondre avec un **paramètre**).<br><br>
    Le premier paramètre, `"USER_CART"` est la clef : c'est ce qui va nous permettre de récupérer le _bean_ dans la vue.<br><br>
    Le deuxième paramètre, `cart`, est le _bean_.

Le _bean_ est maintenant envoyé à la vue, il faut le récupérer avec le méthode `HttpRequest.getAttribute()`. On rajoute le code correspondant juste avant la balise `<!DOCTYPE html>` (c'est-à-dire tout au début de la JSP) :

``` jsp linenums="1"
<% //(1)!
CartBean cart = (CartBean) request.getAttribute("USER_CART");
%>
```

1. Les balises `<% %>` permettent d'ajouter des **_scriptlet_**, c'est-à-dire du code Java à l'intérieur du code HTML.

La classe Java `CartBean` doit donc être importée dans la JSP. Tout en haut du fichier, il faut ajouter :

``` jsp linenums="1"
<%@ page import="fr.univtours.polytech.boutique.model.CartBean"%> <!--(1)!-->
```

1. Ceci s'appelle une directive.

On peut maintenant afficher les informations présentes dans le _bean_ et calculées par la couche métier. Nous allons modifier la partie "Récapitulatif du panier" en ajoutant les appels au _bean_ :

``` jsp linenums="1"
<fieldset>
    <legend>Récapitulatif du panier</legend>
    <table>
        <tr>
            <td>Panier :</td>
            <td><%=cart.getCartPrice()%></td><!--(1)!-->
        </tr>
        <tr>
            <td>Frais livraison :</td>
            <td><%=cart.getShippingCost()%></td>
        </tr>
        <tr>
            <td>Prix total :</td>
            <td><%=cart.getTotalPrice()%></td>
        </tr>
    </table>
</fieldset>
```

1. Le code Java entre `<%= %>` s'appelle une _expression_. Cela permet d'afficher la valeur de l'expression.<br><br>
    Le code `<%=variable %>` est équivalent à `<% out.println(variable) %>`, il affiche le contenu de `variable`.

## 4. Test de la V2.1

Si on teste à nouveau notre application, on a maintenant une erreur au premier affichage 😫 :
<center>
    ![Erreur de la V1.1](../images/boutique_v1.1_erreur.png)
</center>

Cela vient du fait qu'au premier affichage, dans le _scriptlet_ dans la JSP, on récupère dans la requête un objet que nous n'avons pas encore placé ... On obtient donc un `null`, d'où l'erreur :<br>
`java.lang.NullPointerException: Cannot invoke "fr.univtours.polytech.boutique.model.CartBean.getCartPrice()" because "cart" is null`


## 5. Développement de la V2.2

Le problème est qu'au premier affichage, l'objet `CartBean` n'a pas encore été placé dans la requête. Ainsi, dans la JSP, lorsqu'on récupère l'objet présent dans la requête, on récupère et objet `null`, d'où une `NullPointerException` lorsqu'on appelle une méthode sur celui-ci ...

Ce que nous allons faire, c'est de placer un objet `CartBean` vide (mais pas `null`) dans la requête au premier affichage, c'est-à-dire dans la méthode `doGet`. Il suffit donc d'ajouter les deux lignes surlignées dans la méthode `doGet` :

``` java linenums="1" hl_lines="5 6" title="☕ Code Java - StoreServlet"
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

    CartBean cart = new CartBean();//(1)!
    request.setAttribute("USER_CART", cart);//(2)!

    RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
    dispatcher.forward(request, response);
}
```

1. On créée un objet vide, mais pas `null`.
2. Il y a maintenant un objet dans la requête au premier affichage.

## 6. Test de la V2.2

Le premier affichage fonctionne désormais. Il nous reste à mieux initialiser notre formulaire, notamment pour ne plus avoir d'erreur lorsqu'on clique sur <bouton>Valider</bouton> sans avoir changé les valeurs par défaut :
<center>
    ![Affichage de la V1.2](../images/boutique_v1.2.png)
</center>


## 7. Développement de la V2.3

Le problème vient du fait qu'on "_caste_" une chaîne de caractère vide en `Integer` ... Une façon de ne plus avoir ce problème est d'initialiser tous les champs du _bean_ `CartBean` à 0 (on supprime ainsi également l'affichage des _null_ dans la partie panier) :

``` java title="☕ Code Java - classe CartBean" linenums="1"
private Integer penNumber = 0;
private Integer feltNumber = 0;
private Integer rubberNumber = 0;
private Double cartPrice = 0D;
private Double shippingCost = 0D;
private Double totalPrice = 0D;
```

Enfin, dans la vue, il faut récupérer les nombres d'articles sélectionnés. Pour cela, il faut ajouter l'attribut `value` aux balises HTML `<input type="number">`. Cela permet d'avoir une valeur par défaut, qui va être celle stockée dans le _bean_ présent dans la requête. Dans 📄`store.jsp`, la partie _Liste des produits_ devient donc :

``` jsp title="📄 store.jsp" linenums="1"
<form action="store" method="post">
    <fieldset>
        <legend>Liste des produits</legend>
        <table>
            <tr>
                <td>Stylo</td>
                <td><input type="number" name="penNb" value="<%=cart.getPenNumber()%>"/>
                <td></td>
            </tr>
            <tr>
                <td>Feutre</td>
                <td><input type="number" name="feltNb" value="<%=cart.getFeltNumber()%>"/>
                <td></td>
            </tr>
            <tr>
                <td>Gomme</td>
                <td><input type="number" name="rubberNb" value="<%=cart.getRubberNumber()%>"/>
                <td></td>
            </tr>
        </table>
    </fieldset>
    <input type="submit" value="Valider"/>
</form>
```

Vérifie maintenant que l'application est complètement opérationnelle !