---
author: Benoît Domart
title: Conception de l'application
---

# Conception de l'application

Notre application doit respecter le paradigme MVC. Ici, elle est très simple, et respecter ce paradigme va rendre notre code plus complexe. Mais cela nous fera gagner beaucoup de temps par la suite, lorsqu'elle va évoluer ! Si tout le monde respecte les mêmes normes, le code est _rangé_ de la même façon pour tout le monde, et on s'y retrouve donc beaucoup plus rapidement et facilement lorsqu'il faut ajouter une fonctionnalité ou corriger un bug dans une application développée par quelqu'un d'autre.

Pour rappel, voici un schéma de ce paradigme :

<figure markdown>
  ![Schéma MVC](../images/schema_MVC_5.png)
  <figcaption>Schéma du paradigme MVC</figcaption>
</figure>


Pour l'instant, il faut donc déterminer ce qui fait partie du modèle, de la vue, et du controlêur :

1. **Le modèle**<br/>
  Il n'y a pour l'instant qu'une seule classe. C'est le panier. C'est un **_bean_** Java, qui contient plusieurs attributs (et les _getters_/_setters_ associés) :
    * le nombre de stylos commandés,
    * le nombre de feutres commandés,
    * le nombre de gommes commandées,
    * le prix du panier,
    * les frais de livraison,
    * et le prix total de la commande.
2. **La vue**<br/>
  C'est l'affichage de la page Web permettant de sélectionner les quantités désirées de ces trois produits, et qui indique le prix du panier, les frais de livraison et le coût total de la commande.
3. **Le contrôleur**<br/>
  C'est lui qui affiche la vue (avec aucun produit sélectionné initialement) la première fois, et qui met à jour le modèle, avec les informations saisies par l'utilisateur, puis qui ré-affiche la vue mise à jour.<br/>
4. Il y a en fait une 4ème couche à notre application : **La partie métier**<br/>
  Elle est appelée par le contrôleur lorsque l'utilisateur a saisi des données. Elle s'occupe de l'implémentation des trois règles de gestions citées plus haut.<br>
  Dans le paradigme MVC, on peut considérer que cette couche métier fait partie du modèle, puisque son rôle et de mettre ce dernier à jour.
