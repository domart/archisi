2021-2022

- [X] Couper le TD4 en deux TD : TD4 et TD5.
- [X] Parler de partie traitement pour ne pas confondre avec la couche métier.
- [X] Revoir les exemples GestionNotes pour que les services métiers et DOA ne soit pas exactement les mêmes.
- [X] Revoir la DAO manuel pour que toutes les erreurs soient au même niveau (sinon on ne sait pas d'où elles viennent)

- [X] Dans le TD adéquat, être plus explicite sur l'utilisation de la JSTL :
    - [X] Ajouter quelque part
        ```JSP title=""
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        ```
    - [X] Indiquer qu'il faut ajouter les JAR dans WEB-INF/lib.
 
 
 ---------------------------
 2022-2023
 
 - [X] TD1, TD2, TD3, TD4 et TD5 OK.
 - [ ] TD6 : Un peu long
 - [ ] TD6 : Utiliser l'entitymanager avec des annotations. Sinon, ça ne marche pas bien avec le merge.
    => En fait si ! Il faut mettre un singleton.

- [ ] Avant de faire le 1er TP noté, faire un résumé du cours (notamment les annotations à utiliser) pour avoir une archi correcte et fonctionnelle.

- [ ] TD9 : Prévoir deux TPs complets (et c'est même dur de finir).
- [ ] TD10 : A faire sur 2 TPs.

Pour le TD8 noté, donner le DAO.
 
 
 ---------------------------
 2023-2024

 Dans l'ordonnancement des cours, commencer par des TPs pour que tous les élèves soient là, avec leurs ordis portables.

 - [ ] TD1 : Raccourcir le CM sur la partie "généralités sur l'architecture" ? Étudiants peu concentrés ....
 - [X] TD1 : Ajouter qu'il faut installer Maven ! (Avec un MAVEN_HOME à ajouter dans le path).
 - [ ] TD2 : Idem, raccourcir la parie Servlets (ou la laisser, mais la zapper à l'oral).
 - [ ] TD4 (ou ailleurs) : mettre de l'AJAX ? (https://www.jmdoudoux.fr/java/dej/chap-ajax.htm).
 - [ ] TD6 : avec JPA, on peut laisser le "drop and create" et lancer un script sql à chaque publish.

 - [ ] Rajouter quelque part la gestion de la connexion à une appli, avec utilisateur en session.

 - [ ] TD8 (noté) : leur dire de préparer une bdd `exam` et une connexion fonctionnelle vers celle-ci.

 [ ] De manière générale supprimer peut être le TD5 et passer directement sur JPA.

 - [ ] TD9 : Ok pour 2 séances.
 - [ ] TD10 : Rajouter un exemple dans le cours avec PUT et PATCH : Un livre avec des titre/auteur/date/editeur. Effets du PUT et PATCH si juste date/editeur changé.
 - [ ] TD10 : Dans le projet location à récupérer, supprimer l'EJB (n'avoir qu'une seule interface, sans `@Local`).
 - [ ] TD10 : un tout petit peu long (2,5 séances ?)
 - [ ] TD11 : un peu court (1,5 séance ?)

 Pour le TP noté (TD12) : amazon qu'avec des livres ? / préciser quand on met un filtre dans rest sur quoi on filtre.
 https://openlibrary.org/search.json?q=barjavel+nuit+temps
 Possibilité de récupérer une image avec le olid (le author_key), en rajoutant ".jpg"
 - Récupérer les notes (ratings_average).
 - Récupérer le nombre de note.
 - Première date de publication.

apikey dans un fichier properties.
 https://v6.exchangerate-api.com/v6/7b4ffe639515c673bd35ba57/pair/EUR/USD
