# Remarques sur le TD12 noté.

<div style="page-break-after: always;"></div>

## Groupe 1

- Il n'y a pas besoin d'ajouter `jboss-client.jar` dans le *classpath*.
- Dans la couche métier (classe `LocationBusinessImpl`), il serait bien de mutualiser les appels aux 2 APIs Web et la gestion de l'arrondi de la température.
- Dans l'API Web que vous avez créée (classe `RestLocation`), mettez les *path* plutôt en minuscule (`/locations` plutôt que `/location`).


| | Implémentation |
|--|:--:|
| Le champ température n'est pas stocké en BDD | ✅ |
| Ne pas mettre d'intelligence dans le *bean* (par exemple il ne faut pas arrondir la température à cet endroit là) | ✅ |
| Modèle (classe modélisant les résultats des 2 APIs) générées au bon endroit | ✅ |
| `WeatherDao` correct | ✅ |
| Clef API stockée dans une variable statique | ✅ |
| Clef API stockée dans un fichier de propriété | ❌ |
| Couche métier correcte (avec notamment l'arrondi de la température et) | 🟠 |
| Web Service | ✅ |

<div style="page-break-after: always;"></div>

## Groupe 2

- Dans un *bean*, il ne doit pas y avoir d'intelligence. Cela signifie par exemple que ce n'est pas à cet endroit là qu'il faut arrondir la température.
- Les classes `WsAddressResult` et `WsWeatherResult` doivent être dans le package `model`, ou dans un sous-package de celui-ci.
- L'API Web *OpenWeatherMap* est une source de données différentes de l'API Web *adresse* ou de la base de données *location*. Il doit donc y avoir une *DAO* uniquement pour cette nouvelle source de données. Par contre, il n'y a bien qu'une seule brique métier à construire.


| | Implémentation |
|--|:--:|
| Le champ température n'est pas stocké en BDD | ✅ |
| Ne pas mettre d'intelligence dans le *bean* (par exemple il ne faut pas arrondir la température à cet endroit là) | ❌ |
| Modèle (classe modélisant les résultats des 2 APIs) générées au bon endroit | ❌ |
| `WeatherDao` correct | ❌ |
| Clef API stockée dans une variable statique | ❌ |
| Clef API stockée dans un fichier de propriété | ❌ |
| Couche métier correcte (avec notamment l'arrondi de la température et) | ✅ |
| Web Service | ✅ |

<div style="page-break-after: always;"></div>

## Groupe 3

- Il n'y a pas besoin d'ajouter `jackson-annotations-....jar` dans le *classpath*.
    - Par contre il **doit** être présent dans le dossier `WEB-INF/lib`.
- Dans la couche métier (classe `LocationBusinessImpl`), les appels des deux APIs Web sont correctement faits dans la méthode `getLocations`, mais pas dans la méthode `getLocation`.


| | Implémentation |
|--|:--:|
| Le champ température n'est pas stocké en BDD | ✅ |
| Ne pas mettre d'intelligence dans le *bean* (par exemple il ne faut pas arrondir la température à cet endroit là) | ✅ |
| Modèle (classe modélisant les résultats des 2 APIs) générées au bon endroit | ✅ |
| `WeatherDao` correct | ✅ |
| Clef API stockée dans une variable statique | ✅ |
| Clef API stockée dans un fichier de propriété | ❌ |
| Couche métier correcte (avec notamment l'arrondi de la température et) | 🟠 |
| Web Service | ✅ |

<div style="page-break-after: always;"></div>

## Groupe 4

- Dans un *bean*, il ne doit pas y avoir d'intelligence. Cela signifie par exemple que ce n'est pas à cet endroit là qu'il faut arrondir la température.
- Dans la couche métier (classe `LocationBusinessImpl`), il serait bien de mutualiser les appels aux 2 APIs Web et la gestion de l'arrondi de la température.


| | Implémentation |
|--|:--:|:--:|:--:|:--:|:--:|
| Le champ température n'est pas stocké en BDD | ✅ |
| Ne pas mettre d'intelligence dans le *bean* (par exemple il ne faut pas arrondir la température à cet endroit là) | ❌ |
| Modèle (classe modélisant les résultats des 2 APIs) générées au bon endroit | ✅ |
| `WeatherDao` correct | ✅ |
| Clef API stockée dans une variable statique | ❌ |
| Clef API stockée dans un fichier de propriété | ❌ |
| Couche métier correcte (avec notamment l'arrondi de la température et) | 🟠 |
| Web Service | ✅ |

<div style="page-break-after: always;"></div>

## Groupe 5

- Remarque : Dans les boucles `for`, il est préférable de mettre les accolades, même lorsqu'il n'y a qu'une seule ligne (par exemple dans celle dans la classe `LocationsListServlet`).
- La méthode `getTemp` de la classe `LocationBusinessImpl` doit en fait être `private` et appelée dans les méthodes `getLocations` et `getLocation`. C'est dans la couche métier que toutes l'intelligence de l'application doit se trouver.
- Attention, le Web Service n'a pas été codé dans le bon package. Il ne doit pas être dans la partie *modèle*. Il devrait être dans le package `fr.univtours.polytech.locationapp.ws` plutôt que dans `fr.univtours.polytech.locationapp.model.ws`.


| | Implémentation |
|--|:--:|:--:|:--:|:--:|:--:|
| Le champ température n'est pas stocké en BDD | ✅ |
| Ne pas mettre d'intelligence dans le *bean* (par exemple il ne faut pas arrondir la température à cet endroit là) | ✅ |
| Modèle (classe modélisant les résultats des 2 APIs) générées au bon endroit | ✅ |
| `WeatherDao` correct | ✅ |
| Clef API stockée dans une variable statique | ✅ |
| Clef API stockée dans un fichier de propriété | ❌ |
| Couche métier correcte (avec notamment l'arrondi de la température et) | ❌ |
| Web Service | 🟠 |


<div style="page-break-after: always;"></div>
---


| | Groupe 1 | Groupe 2 | Groupe 3 | Groupe 4 | Groupe 5 |
|--|:--:|:--:|:--:|:--:|:--:|
| Le champ température n'est pas stocké en BDD | ✅ | ✅ | ✅ | ✅ | ✅ |
| Ne pas mettre d'intelligence dans le *bean* (par exemple il ne faut pas arrondir la température à cet endroit là) | ✅ | ❌ | ✅ | ❌ | ✅ |
| Modèle (classe modélisant les résultats des 2 APIs) générées au bon endroit | ✅ | ❌ | ✅ | ✅ | ✅ |
| `WeatherDao` correct | ✅ | ❌ | ✅ | ✅ | ✅ |
| Clef API stockée dans une variable statique | ✅ | ❌ | ✅ | ❌ | ✅ |
| Clef API stockée dans un fichier de propriété | ❌ | ❌ | ❌ | ❌ | ❌ |
| Couche métier correcte (avec notamment l'arrondi de la température et) | 🟠 | ✅ | 🟠 | 🟠 | ❌ |
| Web Service | ✅ | ✅ | ✅ | ✅ | 🟠 |
| Note | 18 | 14 | 18 | 15 | 16 |