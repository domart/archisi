---
author: Benoît Domart
title: Correction TD9
tags:
  - 0-correction
---


La correction du TD9 est disponible dans ce dépôt GIT :


<center>
    [Correction TD9 - dépôt GIT](https://scm.univ-tours.fr/domart/td9.git){ .md-button target=_blank }
</center>