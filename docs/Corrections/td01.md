---
author: Benoît Domart
title: Correction TD1
tags:
  - 0-correction
---


La correction du TD1 est disponible dans ce dépôt GIT :


<center>
    [Correction TD1 - dépôt GIT](https://scm.univ-tours.fr/domart/td1.git){ .md-button target=_blank }
</center>