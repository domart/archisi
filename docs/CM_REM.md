``` JSP title="inscription.jsp"
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
```


``` java title="NoteBean"
@GeneratedValue(strategy = GenerationType.IDENTITY)
```

``` java title="NoteBusiness"
public List<NoteBean> getNotesList();
public NoteBean getNote(Integer id);
public void insertNote(NoteBean note);
public Double computeMean(List<NoteBean> notesList);
```

``` java title="NoteDAO"
public List<NoteBean> getNotesList();
public NoteBean getNote(Integer id);
public void insertNote(NoteBean note);
```

``` java title="NoteDAOImpl"
private EntityManager em;

public NoteDAOImpl() {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionNotes");
    this.em = emf.createEntityManager();
}

@Override
public List<NoteBean> getNotesList() {
    Query requete = em.createQuery("select n from NoteBean n");
    return requete.getResultList();
}

@Override
public NoteBean getNote(Integer id) {
    NoteBean note = (NoteBean) em.find(NoteBean.class, id);
    if (null == note) {
        throw new RuntimeException("Cet enregistrement n'existe pas en base : " + id);
    }
    return note;
}

@Override
public void insertNote(NoteBean note) {
    em.getTransaction().begin();
    em.persist(note);
    em.getTransaction().commit();
}
```

``` java title="NoteBusinessImpl"
private NoteDAO dao;

public NoteBusinessImpl() {
    this.dao = new NoteDAOImpl();
}

@Override
public List<NoteBean> getNotesList() {
    return dao.getNotesList();
}

@Override
public NoteBean getNote(Integer id) {
    return dao.getNote(id);
}

@Override
public void insertNote(NoteBean note) {
    dao.insertNote(note);
}

@Override
public Double computeMean(List<NoteBean> notesList) {
    if (null == notesList || notesList.isEmpty()) {
        return null;
    } else {
        Double mean = 0D;
        for (NoteBean note : notesList) {
            mean += note.getNote();
        }
        return mean / notesList.size();
    }
}
```

``` java title="NotesListServlet"
private NoteBusiness business;

@Override
public void init() throws ServletException {
    this.business = new NoteBusinessImpl();
}

@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    List<NoteBean> notesList = business.getNotesList();
    request.setAttribute("NOTES_LIST", notesList);
    final Double mean = business.computeMean(notesList);
    request.setAttribute("MEAN", mean);
    request.getRequestDispatcher("notesList.jsp").forward(request, response);
}
```

``` java title="AddNoteServlet"
private NoteBusiness business;

@Override
public void init() throws ServletException {
    this.business = new NoteBusinessImpl();
}

@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    request.getRequestDispatcher("addNote.jsp").forward(request, response);
}

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    // Récupération des données saisies par l'utilisateur,
    // et enregistrement de celles-ci dans le modèle (c'est-à-dire le bean).
    NoteBean noteBean = new NoteBean();
    String name = request.getParameter("name");
    noteBean.setName(name);
    String firstName = request.getParameter("firstName");
    noteBean.setFirstName(firstName);
    String note = request.getParameter("note");
    noteBean.setNote(Double.parseDouble(note));

    // Enregistrement en BDD :
    business.insertNote(noteBean);

    // Redirection de la requête.
    List<NoteBean> notesList = business.getNotesList();
    request.setAttribute("NOTES_LIST", notesList);
    final Double mean = business.computeMean(notesList);
    request.setAttribute("MEAN", mean);
    request.getRequestDispatcher("notesList.jsp").forward(request, response);
}
```

``` java title="ClientJava"
/**
 * 
 * @return L'EJB session pour manipulation.
 * @throws NamingException Si l'EJB n'est pas trouvé dans l'annuaire JNDI.
 */
private static NoteBusinessRemote getBusiness() throws NamingException {
    Properties jndiProperties = new Properties();
    jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
    jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");
    Context ctx = new InitialContext(jndiProperties);

    String appName = "GestionNotesEAR/";
    String projectName = "GestionNotesEJB/";
    String className = NoteBusinessImpl.class.getSimpleName();
    String remote = NoteBusinessRemote.class.getName();

    String url = "ejb:" + appName + projectName + className + "!" + remote;
    System.out.println("EJB appelé avec : " + url);

    NoteBusinessRemote business = (NoteBusinessRemote) ctx.lookup(url);

    return business;
}
```
