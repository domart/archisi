---
author: Benoît Domart
title: Préparation
---

# TP noté - Préparation


???+tip "À faire avant mercredi 13"
    
    - Préparer une instance sur votre BDD s'appelant `tp_note_1`.
    - Depuis la console d'administration WildFly, créer la datasource `java:/TpNote1`. Pour rappel, voici ce qu'il faut faire :

        1. Sélectionner "MySQL"
        2. Indiquer `TpNote1` comme *Name*, et `java:/TpNote1` comme *JNDI Name*.
        3. Ne rien changer.
        4. Indiquer `jdbc:mysql://localhost:3306/tp_note_1` dans Connection URL (il faut adapter cette URL si le port est différent, et si le schéma créé sur la BDD n'est pas gestion_notes), et saisir le *User Name* et l'éventuel *Password*.
        5. Tester la connexion.
        6. Valider la création de la *datasource*.

        ???+note "Pilote MySql"

            Vous avez déjà créé le pilote MySql, il ne faut donc pas le refaire. La seule opération supplémentaire à faire est ce qui juste ci-dessus.