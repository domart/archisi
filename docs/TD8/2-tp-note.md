---
author: Benoît Domart
title: Énoncé
---

# TP noté - Énoncé

L'objectif ici est de créer le site en ligne (dans une version très simplifiée) d'une bibliothèque.

Il est normal que vous ne puissiez pas tout mettre en place. L'objectif est d'en faire le maximum, en ayant quelque chose de **fonctionnel** et **qui respecte les architectures** que nous avons vues.

Les versions doivent être implémentées **dans l'ordre**. Il faut avoir fini la v1 avant de commencer la v2.

## 1. Version v1

???+success "Modèle"

    Notre modèle va contenir un seul objet :

    <center>
        <table>
            <tr>
                <td>
                    ```mermaid
                    classDiagram
                        BOOK
                        class BOOK{
                            -ID: Integer
                            -TITLE: Varchar
                            -AUTHOR: Varchar
                            -FREE: Boolean
                        }
                    ```
                </td>
            </tr>
        </table>
    </center>

???+success "Règles de gestion"

    Lorsqu'on arrive sur l'application, une page affiche la liste des ouvrages de la bibliothèque, avec leurs différentes informations.

    Pour simplifier, il n'y a qu'un seul utilisateur à notre application;

    - RG1 : Sur les lignes correspondants à un ouvrage disponible, il y a un bouton (ou un lien) "emprunter" permettant d'emprunter l'ouvrage.
    - RG2 : Sur les lignes correspondants à un ouvrage emprunté, il y a un bouton (ou un lien) "rendre" permettant de rendre l'ouvrage.
    - RG3 : Lorsqu'on clique sur le bouton (ou le lien) "emprunter" correspondant à un ouvrage disponible, et qu'il y a strictement moins de trois ouvrages empruntés ($<3$), celui-ci est emprunté, et n'est donc plus disponible.
    - RG4 : Lorsqu'on clique sur le bouton (ou le lien) "emprunter" correspondant à un ouvrage disponible, et qu'il y a trois ouvrages ou plus d'empruntés ($\geq{}3$), celui-ci n'est pas emprunté, et il reste donc disponible.
    - RG5 : Lorsqu'on clique sur le bouton (ou le lien) "rendre" correspondant à un ouvrage emprunté, celui-ci est rendu, et redevient donc disponible.

???+success "Remplissage de la base de données"

    Étant donné que l'option *drop and create* doit être activée (dans le `persistence.xml`), il faut créer une *Servlet*, exécutée par l'url `fillDB`, qui remplit la base de données avec un jeu de test.

## 2. Version v2

Ajouter les écrans permettant de modifier un livre, de supprimer un livre et de créer un livre dans la base de données.